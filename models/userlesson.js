// Database (SQLite)
const db = require('../managers/database-connection').getDB();

class DBUserLesson {
    constructor(userId, lessonId) {
        this.id = 0;
        this.userId = userId;
        this.lessonId = lessonId;
    }

    create(callback) {
        if (this.userId && this.lessonId) {
            let stmt = db.prepare('INSERT INTO UserLesson (UserId, LessonId) VALUES (?, ?)');
            stmt.run(this.userId, this.lessonId, (err, row) => {
                if (err) {
                    throw err;
                }

                callback();
            });
            stmt.finalize();
        }
    }

    getById(id, callback) {
        let stmt = db.prepare('SELECT * FROM UserLesson WHERE UserLessonId = ?');
        stmt.get(id, (err, row) => {
            if (err) {
                throw err;
            }

            if (row) {
                callback(row);
            }
        });
        stmt.finalize();
    }

    getByUserId(id, callback) {
        console.log("email: " + email);
        let stmt = db.prepare('SELECT * FROM UserLesson WHERE UserId = ?');
        stmt.all(id, (err, row) => {
            if (err) {
                throw err;
            }

            if (row) {
                callback(row);
            }

        });
        stmt.finalize();
    }

    getByLessonId(id, callback) {
        console.log("email: " + email);
        let stmt = db.prepare('SELECT * FROM UserLesson WHERE UserId = ?');
        stmt.get(id, (err, row) => {
            if (err) {
                throw err;
            }

            if (row) {
                callback(row);
            }

        });
        stmt.finalize();
    }

    update() {

    }

    remove() {
        if (this.id !== 0) {
            let stmt = db.prepare('DELETE FROM UserLesson WHERE UserLessonId = ?');
            stmt.run(this.id, (err) => {
                if (err) {
                    throw err;
                }
            })
            stmt.finalize();
        }
    }

    removeAllByUserId(userId, callback) {
        let stmt = db.prepare('DELETE FROM UserLesson WHERE UserId = ?');
        stmt.run(userId, (err) => {
            if (err) {
                throw err;
            } else {
                callback();
            }
        })
        stmt.finalize();
    }

    removeAllByLessonId(lessonId, callback) {
        let stmt = db.prepare('DELETE FROM UserLesson WHERE LessonId = ?');
        stmt.run(lessonId, (err) => {
            if (err) {
                throw err;
            } else {
                callback();
            }
        })
        stmt.finalize();
    }
}

module.exports = DBUserLesson;