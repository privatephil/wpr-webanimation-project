// Database (SQLite)
const db = require('../managers/database-connection').getDB();

class DBUser {
    constructor(username, email, password) {
        this.id = 0;
        this.username = username;
        this.email = email;
        this.password = password;
        this.isAdmin = 0;
    }

    create(callback) {
        let stmt = db.prepare('INSERT INTO Users (Username, Password, Email) VALUES (?, ?, ?)');
        stmt.run(this.username, this.password, this.email, (err, row) => {
            if (err) {
                throw err;
            } else {
                callback();
            }
        });
        stmt.finalize();
    }

    getById(id, callback) {
        let stmt = db.prepare('SELECT * FROM Users WHERE UserId = ?');
        stmt.get(id, (err, row) => {
            if (err) {
                throw err;
            }

            if (row) {
                callback(row);
            }
        });
        stmt.finalize();
    }

    getByEmail(email, callback) {
        console.log("email: " + email);
        let stmt = db.prepare('SELECT * FROM Users WHERE Email = ?');
        stmt.get(email, (err, row) => {
            if (err) {
                throw err;
            }

            if (row) {
                callback(row);
            } else {
                callback(null);
            }

        });
        stmt.finalize();
    }

    getAll(callback) {
        let stmt = db.prepare('SELECT * FROM Users');
        stmt.all((err, rows) => {
            if (err) {
                throw err;
            }

            if (rows) {
                callback(rows);
            }

        });
        stmt.finalize();
    }

    update(callback) {
        let stmt = db.prepare('UPDATE Users SET Username = ?, Password = ? WHERE UserId = ?');
        stmt.run(this.username, this.password, this.id, (err, row) => {
            if (err) {
                throw err;
            } else {
                callback();
            }
        });
        stmt.finalize();
    }

    remove(callback) {
        console.log('delete useraccount with id: ' + this.id);
        let stmt = db.prepare('DELETE FROM Users WHERE UserId = ?');
        stmt.run(this.id, (err) => {
            if (err) {
                throw err;
            } else {
                callback();
            }
        })
        stmt.finalize();
    }
}

module.exports = DBUser;