/* model for Elements */
const db = require('../managers/database-connection').getDB();
const fs = require('fs');
const path = require('path');

class Elements {

	constructor(id) {
		this.id = id;
		this.name = null;
		this.path = null;
		this.category = null;

		return this;
	}

    static GetElement(id, callback){
        var sql = `SELECT ObjectId, ObjectName, ObjectFile, Category FROM AnimationObjects WHERE ObjectId = ?`;
        var stmt = db.prepare(sql);
        stmt.get(id , function(err, row) {
                // if any error while executing above query, throw error
                if (err) throw err;
                if(row){
                    // if there is no error, you have the result
                    // console.log(`GET Object with id: ${id}`);
                    callback(row);
                }
                else{
                    console.log('Element not found');
                }
        });
        stmt.finalize();
    }
    
    static GetElements(category, callback){
        var sql = `SELECT ObjectId, ObjectName, ObjectFile, Category FROM AnimationObjects WHERE Category = ?`;
        var stmt = db.prepare(sql);

        stmt.all(category , function(err, rows) {
            // if any error while executing above query, throw error
            if (err) throw err;

            if(rows){
                // if there is no error, you have the result
                // console.log('GET ' + category + ' Objects');
                callback(rows);
            }

        });
        stmt.finalize();
    }

    static DeleteElement(id, callback){
        var sql = `DELETE FROM AnimationObjects WHERE ObjectId = ?`;
        var stmt = db.prepare(sql);
        stmt.run(id);
        stmt.finalize();
        callback();
    } 

    static CreateElement(name, filePath, category, callback){
        var sql = `INSERT INTO AnimationObjects (ObjectName, ObjectFile, Category) VALUES (?, ?, ?)`;
        var stmt = db.prepare(sql);
        stmt.run(name, filePath, category);
        stmt.finalize();
        callback();
    }

    static GetIcons(callback){
        let iconFolders = [];
        // Level 1 Directory /icons/
        let iconsDir = path.join(__dirname , '../static/files/icons')
        let dir = fs.readdirSync(iconsDir, {withFileTypes: true})
        dir.forEach((folder) => {
            // Only read Directories
            if(folder.isDirectory()){
                let iconFolder = {
                    name: folder.name,
                    paths: [],
                };
                // Level 2 Directory /icons/{file}/
                let iconsDir2 = path.join(iconsDir, folder.name)
                let dir2 = fs.readdirSync(iconsDir2, {withFileTypes: true})
                // Only read Files
                if(dir2.length > 0){
                    dir2.forEach((file) =>{
                        if(file.isFile() && !file.name.startsWith('.'))
                        {   
                            // Level 3 File /icons/{file}/{file2}
                            let filePath = path.join(`/files/icons/${folder.name}/`, file.name)
                            iconFolder.paths.push(filePath)
                        }
                    })
                    iconFolders.push(iconFolder);
                }
            }
        })
        console.log(iconFolders)
        callback(iconFolders)
    }

	changeOnDatabase() {
		//statement für WHERE on database abändern usw.
	}

	createOnDatabase() {
		//INSERT INTO.... wird die ID nicht mitübergeben, denn die wäre hier noch 0 oder null und wird von datenbank erzeugt
	}

	deleteFromDatabase() {

	}
}

module.exports = Elements;
