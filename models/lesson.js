/* model for Lesson */
const db = require('../managers/database-connection').getDB();
const md5 = require('md5');

class DBLesson {

	constructor(title, description, creatorId) {
		this.id = 0;

		this.title = title;
		this.description = description;

		this.date = new Date().toJSON();
		this.creatorId = creatorId;
		this.share;
	}

	create(callback) {
		let date = Date.now();
		let rand = Math.random() * (1000 - 1) + 1;
		let randomString = md5(date + rand);

		let stmt = db.prepare('INSERT INTO Lessons (Title, Description, Date, CreatorId, Share) VALUES (?, ?, ?, ?, ?)');
		stmt.run(this.title, this.description, this.date, this.creatorId, randomString, (err, row) => {
			if (err) {
				throw err;
			}

			if (callback) {
				callback();
			}
		});
		stmt.finalize();
	}

	getById(id, callback) {
		let stmt = db.prepare('SELECT * FROM Lessons WHERE LessonId = ?');
		stmt.get(id, (err, row) => {
			if (err) {
				throw err;
			}

			if (row) {
				callback(row);
			}
		});
		stmt.finalize();
	}

	getByShare(share, callback) {
		let stmt = db.prepare('SELECT * FROM Lessons WHERE Share = ?');
		stmt.get(share, (err, row) => {
			if (err) {
				return next(err);
			}

			if (row) {
				callback(row);
			}
		});
		stmt.finalize();
	}

	getByCreatorId(id, callback) {
		let stmt = db.prepare('SELECT * FROM Lessons WHERE CreatorId = ?');
		stmt.all(id, (err, row) => {
			if (err) {
				throw err;
			}

			if (row) {
				callback(row);
			}
		});
		stmt.finalize();
	}

	getLatestByCreatorId(id, callback) {
		let stmt = db.prepare('SELECT * FROM Lessons WHERE CreatorId = ? ORDER BY LessonId DESC LIMIT 1');
		stmt.get(id, (err, row) => {
			if (err) {
				throw err;
			}

			if (row) {
				callback(row);
			}
		});
		stmt.finalize();
	}

	getByCreatorIdSearchSort(id, search, sort, callback) {
		let stmt;

		if (sort === 'Name' || sort === 'Date') {
			if (sort === 'Name') {
				sort = 'Title';
			}

			if (search) {
				stmt = db.prepare('SELECT * FROM Lessons WHERE CreatorId = ? AND Title LIKE "%" ||?|| "%" OR Description LIKE "%" ||?|| "%" ORDER BY ' + sort + ' COLLATE NOCASE ASC');
			} else {
				stmt = db.prepare('SELECT * FROM Lessons WHERE CreatorId = ? ORDER BY ' + sort + ' COLLATE NOCASE ASC');
			}

		} else {
			if (search) {
				stmt = db.prepare('SELECT * FROM Lessons WHERE CreatorId = ? AND Title LIKE "%" ||?|| "%" OR Description LIKE "%" ||?|| "%" ORDER BY LessonId');
			} else {
				stmt = db.prepare('SELECT * FROM Lessons WHERE CreatorId = ? ORDER BY LessonId');
			}

		}

		stmt.all(id, search, search, (err, row) => {
			if (err) {
				throw err;
			}

			if (row) {
				callback(row);
			}
		});
		stmt.finalize();
	}

	getAll(callback) {
		let stmt = db.prepare('SELECT * FROM Lessons');
		stmt.all((err, rows) => {
			if (err) {
				throw err;
			}

			if (rows) {
				callback(rows);
			}
		});
		stmt.finalize();
	}

	update(callback) {
		let stmt = db.prepare('UPDATE Lessons SET Title = ?, Description = ? WHERE LessonId = ?');
		stmt.run(this.title, this.description, this.id, (err, rows) => {
			if (err) {
				throw err;
			} else {
				callback();
			}
		});
		stmt.finalize();
	}

	remove(callback) {
		if (this.id !== 0) {
			console.log('delete animation with id: ' + this.id);
			let stmt = db.prepare('DELETE FROM Lessons WHERE LessonId = ?');
			stmt.run(this.id, (err) => {
				if (err) {
					throw err;
				} else {
					callback();
				}
			})
			stmt.finalize();
		}
	}
}

module.exports = DBLesson; 
