/* model for animation */
const db = require('../managers/database-connection').getDB();
const md5 = require('md5');

class DBAnimation {

	constructor(title, description, creatorId, duration) {
		this.id = 0;

		this.title = title;
		this.description = description;

		this.date = new Date().toJSON();
		this.creatorId = creatorId;

		this.data = '';
		this.share = '';

		this.duration = duration;
	}

	setData(data) {
		this.data = data;
	}

	create(callback) {
		let date = Date.now();
		let rand = Math.random() * (1000 - 1) + 1;
		let randomString = md5(date + rand);

		let stmt = db.prepare('INSERT INTO Animations (Title, Description, Date, potionData, CreatorId, Share, Duration) VALUES (?, ?, ?, ?, ?, ?, ?)');
		stmt.run(this.title, this.description, this.date, this.data, this.creatorId, randomString, this.duration, (err, row) => {
			if (err) {
				throw err;
			}

			if (callback) {
				callback();
			}
		});
		stmt.finalize();
	}

	getById(id, callback) {
		let stmt = db.prepare('SELECT * FROM Animations WHERE AnimationId = ?');
		stmt.get(id, (err, row) => {
			if (err) {
				throw err;
			}

			if (row) {
				callback(row);
			}
		});
		stmt.finalize();
	}

	getByShare(share, callback) {
		let stmt = db.prepare('SELECT * FROM Animations WHERE Share = ?');
		stmt.get(share, (err, row) => {
			if (err) {
				throw err;
			}

			if (row) {
				callback(row);
			}
		});
		stmt.finalize();
	}

	getByCreatorId(id, callback) {
		let stmt = db.prepare('SELECT * FROM Animations WHERE CreatorId = ?');
		stmt.all(id, (err, row) => {
			if (err) {
				throw err;
			}

			if (row) {
				callback(row);
			}
		});
		stmt.finalize();
	}

	getByCreatorIdSearchSort(id, search, sort, callback) {
		let stmt;

		if (sort === 'Name' || sort === 'Date') {
			if (sort === 'Name') {
				sort = 'Title';
			}

			if (search) {
				stmt = db.prepare('SELECT * FROM Animations WHERE CreatorId = ? AND Title LIKE "%" ||?|| "%" OR Description LIKE "%" ||?|| "%" ORDER BY ' + sort + ' COLLATE NOCASE ASC');
			} else {
				stmt = db.prepare('SELECT * FROM Animations WHERE CreatorId = ? ORDER BY ' + sort + ' COLLATE NOCASE ASC');
			}

		} else {
			if (search) {
				stmt = db.prepare('SELECT * FROM Animations WHERE CreatorId = ? AND Title LIKE "%" ||?|| "%" OR Description LIKE "%" ||?|| "%" ORDER BY AnimationId');
			} else {
				stmt = db.prepare('SELECT * FROM Animations WHERE CreatorId = ? ORDER BY AnimationId');
			}
		}

		stmt.all(id, search, search, (err, row) => {
			if (err) {
				throw err;
			}

			if (row) {
				callback(row);
			}
		});
		stmt.finalize();
	}

	getLatestByCreatorId(id, callback) {
		let stmt = db.prepare('SELECT * FROM Animations WHERE CreatorId = ? ORDER BY AnimationId DESC LIMIT 1');
		stmt.get(id, (err, row) => {
			if (err) {
				throw err;
			}

			if (row) {
				callback(row);
			}
		});
		stmt.finalize();
	}

	getAll(callback) {
		let stmt = db.prepare('SELECT * FROM Animations');
		stmt.all((err, rows) => {
			if (err) {
				throw err;
			}

			if (rows) {
				callback(rows);
			}
		});
		stmt.finalize();
	}

	getByCreatorNotInLesson(creatorId, lessonId, callback) {
		// old query: 'SELECT t1.AnimationId, t1.Title, t1.Description, t1.Date, t1.CreatorId FROM Animations t1 LEFT JOIN AnimationLesson t2 ON t1.AnimationId = t2.AnimationId WHERE t1.CreatorId = ? AND (t2.LessonId != ? OR t2.LessonId IS NULL)'

		let stmt = db.prepare('SELECT t1.AnimationId, t1.Title, t1.Description, t1.Date, t1.Duration, t1.CreatorId FROM Animations t1 WHERE t1.CreatorId = ? AND t1.AnimationId NOT IN (SELECT AnimationId FROM AnimationLesson t2 WHERE LessonId = ?)');
		stmt.all(creatorId, lessonId, (err, rows) => {
			if (err) {
				throw err;
			}

			if (rows) {
				callback(rows);
			}
		});
		stmt.finalize();
	}

	update(callback) {
		let stmt = db.prepare('UPDATE Animations SET Title = ?, Description = ?, potionData = ?, Duration = ? WHERE AnimationId = ?');
		stmt.run(this.title, this.description, this.data, this.duration, this.id, (err, rows) => {
			if (err) {
				throw err;
			}

			if (callback) {
				callback();
			}
		});
		stmt.finalize();
	}

	remove(callback) {
		let stmt = db.prepare('DELETE FROM Animations WHERE AnimationId = ?');
		stmt.run(this.id, (err) => {
			if (err) {
				throw err;
			} else {
				callback();
			}
		})
		stmt.finalize();

	}
}

module.exports = DBAnimation; 
