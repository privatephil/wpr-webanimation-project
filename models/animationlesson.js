/* model for Lesson */
const db = require('../managers/database-connection').getDB();

class DBAnimationLesson {

	constructor(lessonId, animationId) {
		this.id = 0;

		this.lessonId = lessonId;
		this.animationId = animationId;
	}

	create(callback) {
		let stmt = db.prepare('INSERT INTO AnimationLesson (AnimationId, LessonId) VALUES (?, ?)');
		stmt.run(this.animationId, this.lessonId, (err, row) => {
			if (err) {
				throw err;
			}

			if (callback) {
				callback();
			}
		});
		stmt.finalize();
	}

	getById(id, callback) {
		let stmt = db.prepare('SELECT * FROM AnimationLesson WHERE AnimationLessonId = ?');
		stmt.get(id, (err, row) => {
			if (err) {
				throw err;
			}

			if (row) {
				callback(row);
			}
		});
		stmt.finalize();
	}

	getAnimationsByLessonId(id, callback) {
		let stmt = db.prepare('SELECT * FROM AnimationLesson t1 LEFT JOIN Animations t2 ON t1.AnimationId = t2.AnimationId WHERE t1.LessonId = ?');
		stmt.all(id, (err, rows) => {
			if (err) {
				throw err;
			}

			if (rows) {
				callback(rows);
			}
		});
		stmt.finalize();
	}

	getLessonsByAnimationId(id, callback) {
		let stmt = db.prepare('SELECT * FROM AnimationLesson t1 LEFT JOIN Animations t2 ON t1.AnimationId = t2.AnimationId WHERE t1.AnimationId = ?');
		stmt.all(id, (err, rows) => {
			if (err) {
				throw err;
			}

			if (rows) {
				callback(rows);
			}
		});
		stmt.finalize();
	}

	getAll(callback) {
		let stmt = db.prepare('SELECT * FROM AnimationLesson');
		stmt.all((err, rows) => {
			if (err) {
				throw err;
			}

			if (rows) {
				callback(rows);
			}
		});
		stmt.finalize();
	}

	update() {

	}

	removeBySetIds(callback) {
		let stmt = db.prepare('DELETE FROM AnimationLesson WHERE AnimationId = ? AND LessonId = ?');
		stmt.run(this.animationId, this.lessonId, (err) => {
			if (err) {
				throw err;
			} else {
				callback();
			}
		})
		stmt.finalize();
	}

	removeAllByAnimationId(animationId, callback) {
		let stmt = db.prepare('DELETE FROM AnimationLesson WHERE AnimationId = ?');
		stmt.run(animationId, (err) => {
			if (err) {
				throw err;
			} else {
				callback();
			}
		})
		stmt.finalize();
	}

	removeAllByLessonId(lessonId, callback) {
		let stmt = db.prepare('DELETE FROM AnimationLesson WHERE LessonId = ?');
		stmt.run(lessonId, (err) => {
			if (err) {
				throw err;
			} else {
				callback();
			}
		})
		stmt.finalize();
	}

	remove(callback) {
		if (this.id !== 0) {
			let stmt = db.prepare('DELETE FROM AnimationLesson WHERE AnimationLessonId = ?');
			stmt.run(this.id, (err) => {
				if (err) {
					throw err;
				} else {
					callback();
				}
			})
			stmt.finalize();
		}
	}
}

module.exports = DBAnimationLesson; 
