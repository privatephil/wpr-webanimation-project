// Requires
let express = require('express');
let path = require('path');

let sessionManger = require('./managers/session-manager');
let parserManager = require('./managers/parsing-manager');
let dbConnectionManager = require('./managers/database-connection');

// Initialisations
sessionManger.initSession();
parserManager.initParser();
dbConnectionManager.initDB();

// Configuration
let app = express();
const port = 8888;

// Use Session
app.use(sessionManger.getSession());

// Define path for static files and view engine for templates
app.use(express.static(path.join(__dirname, '/static')));
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// Routes
let indexRoute = require('./controllers/start.js')
let loginRoute = require('./controllers/login.js');
let logoutRoute = require('./controllers/logout.js');
let registerRoute = require('./controllers/register.js');
let lessonsRoute = require('./controllers/lessons.js');
let animationsRoute = require('./controllers/animations.js');
let viewRoute = require('./controllers/view.js');
let profileRoute = require('./controllers/profile.js');
let adminRoute = require('./controllers/admin.js');
let elementsRoute = require('./controllers/elements.js');
let impressumRoute = require('./controllers/impressum.js');
let contactRoute = require('./controllers/contact.js');
let agbRoute = require('./controllers/agb.js')


// Pseudo routes
let delAccRoute = require('./controllers/delete-account.js');
let delLessRoute = require('./controllers/delete-lesson.js');
let delAnimRoute = require('./controllers/delete-animation.js');
let adminDelAccRoute = require('./controllers/admin-delete-account.js');
let adminDelLessRoute = require('./controllers/admin-delete-lesson.js');
let adminDelAnimRoute = require('./controllers/admin-delete-animation.js');
let changeDescRoute = require('./controllers/change-desc.js');
let addAnimRoute = require('./controllers/add-animation.js');
let removeAnimRoute = require('./controllers/remove-animation.js');


//Start with redirect if not logged in
app.use('/', indexRoute);
app.use('/login', loginRoute);
app.use('/logout', logoutRoute);
app.use('/register', registerRoute);
app.use('/lessons', lessonsRoute);
app.use('/animations', animationsRoute);
app.use('/view', viewRoute);
app.use('/profile', profileRoute);
app.use('/admin', adminRoute);
app.use('/elements', elementsRoute);
app.use('/impressum', impressumRoute);
app.use('/contact', contactRoute);
app.use('/agb', agbRoute);


// Pseudo ressources
app.use('/delete-account', delAccRoute);
app.use('/delete-lesson', delLessRoute);
app.use('/delete-animation', delAnimRoute);
app.use('/admin-delete-account', adminDelAccRoute);
app.use('/admin-delete-lesson', adminDelLessRoute);
app.use('/change-desc', changeDescRoute);
app.use('/add-animation', addAnimRoute);
app.use('/remove-animation', removeAnimRoute);
app.use('/admin-delete-animation', adminDelAnimRoute);


//Default error handling
app.use(function errorHandler(err, req, res, next) {
	console.log('An error occured!');

	if (res.headersSent) {
		return next(err);
	}

	console.log(err);
	res.status(500);
	//res.render('error', { error: err });
	res.render('redirects/erroroccurred');
}
);

// Start Webapplication
const server = app.listen(port, () => console.log('ready on port ' + port));

process.on('SIGTERM', () => {
	console.log('Closing http server.');
	server.close(() => {
		console.log('Http server closed.');

		require('database-connection').close();
	});
});