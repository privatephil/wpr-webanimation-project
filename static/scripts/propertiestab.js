// ******************** PROPERTIES TAB ********************




// ******************** Variables (for Dropdown Options) ********************
var fontFamilyOptions = ['Amatic SC', 'Arial', 'Trebuchet MS, Helvetica, sans-serif', 'Lucida Sans Unicode, Lucida Grande, sans-serif', 'Tahoma, Geneva, sans-serif', 'Verdana, Geneva, sans-serif' ,'Times New Roman, Times, serif', 'Georgia, serif', 'Palatino Linotype, Book Antiqua, Palatino, serif', 'Lucida Console, Monaco, monospace', 'Impact, Charcoal, sans-serif', 'Courier New, Courier, monospace'];
//var fontFamilyOptions  = ['Amatic SC', 'Times New Roman, Times, serif', 'Georgia, serif', 'Arial', 'Trebuchet MS, Helvetica, sans-serif', 'Courier New, Courier, monospace', 'Impact, Charcoal, sans-serif'];
var textAlignOptions    = ['center', 'left', 'right'];




// ************************************************************
//show list of elements or properties depending on selection/deselection of Canvas Objects
function updatePropertiesTab(choice, target) {
  var selectedObjects = shapesList.getSelected(); //getSelectedCanvasObjects();
  var numberOfObjects = selectedObjects.length; //marked Objects after the click
  // console.log('Switch: ' + choice);

  switch (choice) {
    case 1:     //call when creating an Object

      if (numberOfObjects == 0) {
        resetElementsList();
      }
      break;  //End of Case 1

    case 2:     //call when Selecting an Object

      if (numberOfObjects == 1) {
        showProperties(target);
      } else {

        setElementsList(selectedObjects);
      }
      break;  //End of Case 2

    case 3:     //call when De-Selecting(+STRG) or deleting an Object 
      if (numberOfObjects == 0) { //if there is no object selected anymore -> reset list completely
        resetElementsList();
      } else if (numberOfObjects == 1) { //if there is just one object -> show properties
        showProperties(selectedObjects[0].element);
      } else { //if there are more -> show list of selected elements
        setElementsList(selectedObjects);
      }
      break;  //End of Case 3

    default:
      return;
  }
}




// ******************** Elements List ********************
//Show List of all Elements
function resetElementsList() {
  var collection = document.getElementById('tabItems');  //div that the Item has to be placed in
  $(collection).empty(); //first delete everything from the collection
  document.getElementById('propertiesTitle').textContent = 'Objects';

  //create all List Elements
  var objs = shapesList.getAll(); //getCanvasObjects();
  if (objs.length == 0) {
    var container = document.createElement('div');
    container.classList.add('collection-item');
    container.textContent = 'There are no Objects in the Scene.';
    collection.appendChild(container);
  } else {
    for (var i = 0; i < objs.length; i++) {
      createElementsEntry(objs[i].element.id); //create an entry
    }
  }
  createColorPicker();
}

//Show List of all SELECTED Elements
function setElementsList(selectedObjects) {
  var collection = document.getElementById('tabItems');  //div that the Item has to be placed in
  $(collection).empty(); //first delete everything from the collection
  document.getElementById('propertiesTitle').textContent = 'Objects';

  //create all List Elements
  for (var i = 0; i < selectedObjects.length; i++) {
    //console.log('ID: '+i);
    createElementsEntry(selectedObjects[i].element.id); //create an entry
  }
}

//Create a single element in the Elements List
function createElementsEntry(id) {
  var collection = document.getElementById('tabItems');  //div that the Item has to be placed in

  var container = document.createElement('a');
  container.setAttribute('href', 'javascript:void(0)');
  container.setAttribute('onclick', 'clickOnElementInTab(\'' + id + '\')');
  container.classList.add('collection-item');
  //Add Elements to Document
  container.textContent = id;

  var element = document.getElementById(id);
  var src = element.src;
  if (src != undefined) {
    var thumbnail = document.createElement('img');

    thumbnail.setAttribute('src', document.getElementById(id).src);
    thumbnail.setAttribute('width', '25px');
    thumbnail.setAttribute('height', '25px');
    thumbnail.style.marginRight = '12.5px';

    thumbnail.classList.add('floatLeft');
    container.appendChild(thumbnail);
  }
  else if(element.classList.contains('textField')) {
    var thumbnail = document.createElement('i');
    thumbnail.classList.add('material-icons');
    thumbnail.textContent = 'text_fields';
    thumbnail.classList.add('floatLeft');
    thumbnail.style.marginRight = '12.5px';
    thumbnail.style.fontSize = '25px';
    container.appendChild(thumbnail);
  }





  collection.appendChild(container);
}

//when clicking an element in tab select the canvasObject and show the respective Properties
function clickOnElementInTab(id) {
  console.log('ObjectID: ' + id);
  var element = document.getElementById(id);

  selectShapesById(new Event('click'), id);

  showProperties(element);
}




// ******************** Properties List ********************
//when an element is selected, show a list of the respective properties
function showProperties(target) 
{
  var collection = document.getElementById('tabItems');  //div that the Item has to be placed in
  $(collection).empty();  //first delete everything from the tab

  var id = target.id;
  var style = target.style; //all inline-styles

  //var computedStyle = getComputedStyle(target); //styles that are part of the css classes (only get!)
  //set Title
  var propertiesTitle = document.getElementById('propertiesTitle');
  propertiesTitle.textContent = id;

  var element = document.getElementById(id);


  //Back Button
  var icon = document.createElement('i');
  icon.classList.add('white-text');
  icon.classList.add('material-icons');
  icon.classList.add('left');
  icon.textContent = 'arrow_back';
  icon.style.fontSize = '40px';
  icon.setAttribute('onClick', 'deselectAllShapes()');
  //icon.style.marginTop = '2px';
  propertiesTitle.appendChild(icon);
  //Thumbnails
  var src = element.src;
  if (src != undefined) { //create Thumbnail
    var thumbnail;
    var thumbnail = document.createElement('img');
    //console.log(src);
    thumbnail.setAttribute('src', src);

    thumbnail.setAttribute('width', '40px');
    thumbnail.setAttribute('height', '40px');
    thumbnail.style.marginRight = '20px';

    thumbnail.classList.add('floatLeft');
    propertiesTitle.appendChild(thumbnail);

  }

  else if(element.classList.contains('textField')) {
    var thumbnail = document.createElement('i');
    thumbnail.classList.add('material-icons');
    thumbnail.textContent = 'text_fields';
    thumbnail.classList.add('floatLeft');
    thumbnail.setAttribute('width', '40px');
    thumbnail.setAttribute('height', '40px');
    thumbnail.style.fontSize = '40px';
    thumbnail.style.marginRight = '20px';
    propertiesTitle.appendChild(thumbnail);
  }

  //Delete Button
      //<a id="deleteKeyframe" class="btn btn-small disabled tooltipped" data-position='top' data-tooltip='Delete Keyframe' style="float: right; margin-bottom: -12px;background-color: #ef5350"><i class="material-icons">delete</i></a>
    createDeleteButton(id, propertiesTitle);


  //Create Properties Entries

  if(target.classList.contains('textField')){
    showTextProperties(target);
  }

  createSubTitleElement('Transformations');
  //Translation X
  createPropertiesEntry('left', 'Translation X:', target.style.left, target, 'number', '%', 0.1, 0, 100); //create an entry with the property and the value
  //Translation Y
  createPropertiesEntry('top', 'Translation Y:', target.style.top, target, 'number', '%', 0.1, 0, 100); //create an entry with the property and the value
  //Scale X
  createPropertiesEntry('width', 'Width:', target.style.width, target, 'number', '%', 0.1, 0, 100); //create an entry with the property and the value
  //Scale Y
  createPropertiesEntry('height', 'Height:', target.style.height, target, 'number', '%', 0.1, 0, 100); //create an entry with the property and the value
  //Rotation
  createPropertiesEntry('rot', 'Rotation:', Math.degrees(parseFloat(target.getAttribute('data-angle'))) || 0, target, 'number', '', 1, -3600, 3600); //create an entry with the property and the value
  //Opacity
  createPropertiesEntry('opacity', 'Opacity:', target.style.opacity, target, 'number', '', 0.1, 0, 1); //create an entry with the property and the value
  //Z-Index
  createPropertiesEntry('z-index', 'Z-Index:', target.style.zIndex, target, 'number', '', 1, 1, 100); //create an entry with the property and the value


}

//if selected element is a textfield show more text relate properties
function showTextProperties(target) {
  createSubTitleElement('Text Styling');

  //Font Family:
  createPropertyDropdown('font-family', 'Font Family:', fontFamilyOptions, target);
  //Color:
  createPropertiesEntry('color', 'Font Color:', target.color, target, 'color', ''); //create an entry with the property and the value
    //Size:
    createPropertiesEntry('font-size', 'Font Size:', target.style.fontSize, target, 'number', '%', 1, 0, 50); //create an entry with the property and the value
  //Bold (font-weight):
  createPropertyCheckbox('font-weight', 'Bold:', target, 900);
  //Italic (font-style):
  createPropertyCheckbox('font-style', 'Italic:', target, 'italic');
  //Underline (text-decoration):
  createPropertyCheckbox('text-decoration', 'Underline:', target, 'underline');
  //Text-Align(text-align):
  createPropertyDropdown('text-align', 'Text Alignment:', textAlignOptions, target);

}

//if something was changed in the canvas (e.g. transform via transform tool), update the respective value in the Properties Tab
function updateValueInTab(property, value) {
  let input = document.getElementById(property + '-input');
  if(input != null)  input.value = value;
  else console.log(property +  '-property does not exist in this element.');
}

//update all values in the Properties Tab (e.g. when time value is changed)
function updateAllValues() {
  if(shapesList.getSelected().length > 0) {
    let element = shapesList.getSelected()[0].element;
    console.log('THE ID IS: ' + element.id);
    //LEFT
    //document.getElementById('left-input').value = element.style.left.substring(0, element.style.left.length-1);
    document.getElementById('left-input').value = parseFloat(element.style.left).toFixed(2);
    //TOP
    document.getElementById('top-input').value = parseFloat(element.style.top).toFixed(2);
    //WIDTH
    document.getElementById('width-input').value = parseFloat(element.style.width).toFixed(2);
    //HEIGHT
    document.getElementById('height-input').value = parseFloat(element.style.height).toFixed(2);
    //OPACITY
    document.getElementById('opacity-input').value = parseFloat(element.style.opacity).toFixed(2);
    //ROTATION
    let regex = new RegExp('rotate.([+-]?([0-9]*[.])?[0-9]+)rad');
    let res = regex.exec(element.style.transform);
    if(res) {var thisRotation = parseFloat(res[1]);}
		else {var thisRotation = 0;}		
    document.getElementById('rot-input').value = Math.degrees(thisRotation).toFixed(2);

    //set the transform tool back on the shape
    transformToolSelect();
  }
}

function createDeleteButton(id, parent)
{
  var deleteButton = document.createElement('a');
  deleteButton.classList.add('btn');
  deleteButton.classList.add('floatRight');
  deleteButton.setAttribute('id', 'deleteShapeButton');
  deleteButton.setAttribute('width', '40px');
  deleteButton.setAttribute('height', '40px');
  deleteButton.setAttribute('onclick', 'deleteSelectedShapes()');
  deleteButton.style.backgroundColor = '#EF5350';
  deleteButton.innerHTML = '<i class="material-icons">delete</i>';
  
  parent.appendChild(deleteButton);
}


// ******************** Create Input Elements ********************
//Create a single Input-Element for the Properties Tab -> this can be different types of input
function createPropertiesEntry(property, displayedName, propertyValue, target, inputType, valueUnit, step, min, max) {
  var collection = document.getElementById('tabItems');  //div that the Item has to be placed in

  //create Elements
  var upperDiv = document.createElement('div');
  var lowerDiv = document.createElement('div');
  var leftSide = document.createElement('div');
  var rightSide = document.createElement('div');
  var input = document.createElement('input');
  //Style Elements
  upperDiv.classList.add('collection-item');
  lowerDiv.classList.add('row');
  lowerDiv.style.margin = '0';
  leftSide.classList.add('col');
  leftSide.classList.add('m5');
  leftSide.style.marginTop = '12px';
  if(inputType == 'color') rightSide.style.marginTop = '12px';
  rightSide.classList.add('col');
  rightSide.classList.add('m7');
  if (valueUnit.length!= 0) {
    propertyValue = propertyValue.substring(0, propertyValue.length - valueUnit.length); //e.g. remove the '%' to make it fit the Number input
  }
  input.setAttribute('onInput', 'setPropertyValue(this.name, this.value, \'' + target.id + '\', \'' + valueUnit + '\', ' + min + ', ' + max + ')');
  input.setAttribute('type', inputType);
  input.setAttribute('value', propertyValue + '');
  input.setAttribute('name', property + '');
  input.setAttribute('id', property + '-input');
  input.style.color = '#d4d4d4';
  if (step != null) input.setAttribute('step', step);

  //Add Elements to Document
  leftSide.textContent = displayedName;
  rightSide.appendChild(input);
  lowerDiv.appendChild(leftSide);
  lowerDiv.appendChild(rightSide);
  upperDiv.appendChild(lowerDiv);
  collection.appendChild(upperDiv);
}

//Create the ColorPicker which changes the Background Color
function createColorPicker() {
  var collection = document.getElementById('tabItems');  //div that the Item has to be placed in
  var canvas = document.getElementById('canvas');

  //create Elements
  var upperDiv = document.createElement('div');
  var lowerDiv = document.createElement('div');
  var leftSide = document.createElement('div');
  var rightSide = document.createElement('div');
  var input = document.createElement('input');
  //Style Elements
  upperDiv.classList.add('collection-item');
  lowerDiv.classList.add('row');
  leftSide.classList.add('col');
  leftSide.classList.add('m5');
  rightSide.classList.add('col');
  rightSide.classList.add('m7');

  input.setAttribute('onInput', 'setBackgroundColor(this.value)');
  input.setAttribute('type', 'color');
   input.setAttribute('value', bgColor);
  input.setAttribute('name', 'bgColor');
  input.setAttribute('id', 'bgColor-input');

  //Add Elements to Document
  leftSide.textContent = 'Background Color:';
  rightSide.appendChild(input);
  lowerDiv.appendChild(leftSide);
  lowerDiv.appendChild(rightSide);
  upperDiv.appendChild(lowerDiv);
  collection.appendChild(upperDiv);
}

//Create a Select-Input-Element -> Options have to be saved as Arrays
function createPropertyDropdown(property, displayedName, options, target) {
  var collection = document.getElementById('tabItems');  //div that the Item has to be placed in

  //create Elements
  var upperDiv = document.createElement('div');
  var lowerDiv = document.createElement('div');
  var leftSide = document.createElement('div');
  var rightSide = document.createElement('div');
  var select = document.createElement('select');
  //Style Elements
  upperDiv.classList.add('collection-item');
  lowerDiv.classList.add('row');
  lowerDiv.style.margin = '0';
  leftSide.classList.add('col');
  leftSide.classList.add('m5');
  leftSide.style.marginTop = '12px';
  rightSide.classList.add('col');
  rightSide.classList.add('m7');
  rightSide.classList.add('input-field');
  select.classList.add('browser-default');
  select.style.height = '30px';
  select.setAttribute('onChange', 'setPropertyValue(\'' + property + '\', this.value, \'' + target.id + '\', \'\', 0, 100)');
  select.setAttribute('id', property+'-input');

  for(let i = 0; i<options.length; i++){
    var opt = document.createElement('option');
    //Value is what should be used!
    //textContent is what should be displayd!
    if(options[i].includes(',')) {
      let sub = options[i].substring(0, options[i].indexOf(','));
      opt.textContent = sub;
    }  
    else {
      opt.textContent = options[i];
    }
    if(property == 'font-family') opt.style.fontFamily = options[i];
    
    opt.value = options[i]; 
    select.appendChild(opt);
  }

  //Add Elements to Document
  leftSide.textContent = displayedName;
  rightSide.appendChild(select);
  lowerDiv.appendChild(leftSide);
  lowerDiv.appendChild(rightSide);
  upperDiv.appendChild(lowerDiv);
  collection.appendChild(upperDiv);
}

//Create a Checkbox-Input-Element for boolean properties
function createPropertyCheckbox(property, displayedName, target, valueChecked) {
  var collection = document.getElementById('tabItems');  //div that the Item has to be placed in

  //create Elements
  var upperDiv = document.createElement('div');
  var lowerDiv = document.createElement('div');
  var leftSide = document.createElement('div');
  var rightSide = document.createElement('div');
  var formElement = document.createElement('form');
  var input = document.createElement('input');
  var label = document.createElement('label');
  var span = document.createElement('span');
  let elementProperty = $('#' + target.id).css(property);
  //Style Elements
  upperDiv.classList.add('collection-item');
  lowerDiv.classList.add('row');
  lowerDiv.style.margin = '0';
  leftSide.classList.add('col');
  leftSide.classList.add('m5');
  leftSide.style.marginTop = '12px';
  rightSide.classList.add('col');
  rightSide.classList.add('m7');
  rightSide.style.marginTop = '12px';
  input.setAttribute('type', 'checkbox');
console.log('prop' + + property + ' elementProperty' + elementProperty + ' and valueChecked' + valueChecked);

  if(elementProperty != valueChecked) {input.checked = false;}
  else input.checked = true;

  input.setAttribute('onclick', 'setCheckboxProperty(this, \''+property+'\', \''+target.id+'\', \''+valueChecked+'\')');
  input.setAttribute('id', property+'-input');


  //Add Elements to Document
  leftSide.textContent = displayedName;
  rightSide.appendChild(label);
  label.appendChild(input);
  label.appendChild(span);
  lowerDiv.appendChild(leftSide);
  lowerDiv.appendChild(rightSide);
  upperDiv.appendChild(lowerDiv);
  collection.appendChild(upperDiv);
}

//Create the "Update Keyframe Values" Button
function AddUpdateKeyframeButton()
{
  if(document.getElementById('updateKfValues') == null)
  {
    var collection = document.getElementById('tabItems');  //div that the Item has to be placed in
    //create Elements
    var upperDiv = document.createElement('div');
    //Style Elements
    upperDiv.setAttribute('id', 'updateKfValues');
    upperDiv.classList.add('waves-effect');
    upperDiv.classList.add('waves-light');
    
    upperDiv.classList.add('collection-item');
    upperDiv.classList.add('updateKfValuesStyle');
  
    //Add Elements to Document
    upperDiv.textContent = 'Update Keyframe Values';
    collection.insertBefore(upperDiv, collection.firstChild);
    $(upperDiv).bind('click', updateSelectedKeyframe);
    

  }
}

//Remove the "Update Keyframe Values" Button
function RemoveUpdateKeyframeButton()
{
  console.log('and remove!');
  let element = document.getElementById('updateKfValues');
  if(element != null) element.remove();
  

}

//Create a Title (e.g. "Text Styling") -> NO INPUT
function createSubTitleElement(textContent) {
  let collection = document.getElementById('tabItems');
  let upperDiv = document.createElement('div');
  upperDiv.classList.add('collection-itemNoHover');
  upperDiv.classList.add('updateKfValuesStyle');
  upperDiv.textContent = textContent;
  collection.appendChild(upperDiv);
}




// ********** Set/Update Values **********
//update propertity value of Canvas Element when value is changed in Properties Tab
function setPropertyValue(property, newValue, targetID, valueUnit, min, max) {
  var inputElement = document.getElementById(property + '-input');
  console.log('NEW VALUE!' + newValue);

  if (newValue < min) { //the entered value was too small!
    console.log('too small');
    newValue = min;
    inputElement.value = min;
  }

  else if (newValue > max) { //the entered value was too small!
    console.log('too high');
    newValue = max;
    inputElement.value = max;
  }

  console.log('NEW VALUE IS NOW:' + newValue);

  if (property != 'rot') {
    newValue = newValue + valueUnit;
    if (newValue != '') {
      // var target = document.getElementById(targetID);

      $('#' + targetID).css(property, newValue);
      transformToolSelect();
      console.log('DONE');
      if (property == 'z-index') {
        let shape = shapesList.getShape(targetID);
        shape.setZIndex(newValue);
      }
    }
  } else {  //extra Case because Rotation works differently
    let target = document.getElementById(targetID);
    let angleInRad = Math.radians(newValue);
    target.style.transform = 'translate(-50%, -50%) rotate(' + angleInRad + 'rad' + ')';
    target.setAttribute('data-angle', angleInRad);
    transformToolSelect();
    console.log('DONE');
  }
}

//update the color of the background/canvas
function setBackgroundColor(color)
{
  console.log('NEW COLOR' + color);
  document.getElementById('canvas').style.backgroundColor = color;
  shapesList.backgroundColor = color;
}

//update boolean Properties on Checbox Click
function setCheckboxProperty(checkbox, property, targetID, targetValue)
{
  if(checkbox.checked) {
    console.log('Set ' + property + 'to ' + targetValue);
    $('#' + targetID).css(property, targetValue);
  }
  else {
    console.log('Set ' + property + 'to Normal');

    $('#' + targetID).css(property, '');
  }
}

//check if entered value is between min and max
function checkForMinAndMax(value, min, max, inputElement) {
  let inBounds = true;
  if (value < min) { //the entered value was too small!
    console.log('too small');
    value = min;
    inputElement.value = min;
    inBounds = false;
  }

  else if (value > max) { //the entered value was too small!
    console.log('too high');
    value = max;
    inputElement.value = max;
    inBounds = false;
  }
  return inBounds;
}




// ******************** PROPERTIES TAB END ********************