var numberId = 0;
var canvasObjects = [];
var tabObjects = [];

window.onload = function() {
  interact('.draggableObj') //.selectedObj // ungewollte Moveprotection als Checkbox
    .draggable({
      onmove: window.dragMoveListener,
      modifiers: [
        interact.modifiers.restrict({
          restriction: 'parent',
          elementRect: {
            top: 0,
            left: 0,
            bottom: 1,
            right: 1
          }
        })
      ]
    }).on('dragstart', selectCanvasObject);

    //Drag and Drop Objects from Menu into Canvas
    interact('.dragDropObj')
    .draggable({
      onmove: window.dragMoveListener }) //use the same dragMoveListener
    .on('move', function (event) {
      var interaction = event.interaction
      // if the pointer was moved while being held down
      // and an interaction hasn't started yet
      if (interaction.pointerIsDown && !interaction.interacting()) {
        //var original = event.target; //BUG: when clicked, Child Element is clicked sometimes (the circle)

        //Create a Dummy object at the cursor position
        var dummy;
        dummy = document.createElement('div'); 
        dummy.classList.add('circle2'); //circle for testing
        document.body.appendChild(dummy);
        dummy.setAttribute('id', 'dropObj'); //this ID tells the dropzone that the object can be dropped
        dummy.style.left = (event.x - dummy.offsetWidth/2) + 'px'; //set ne Element to cursor position //HIER BITTE MITÜBERLEGEN, DA DIES NICHT ZUR POSITION AUS DEM DRAGMOVELISTENER PASST!
        dummy.style.top = (event.y - dummy.offsetHeight/2) + 'px';
        
        // start a drag interaction targeting the dummy
        interaction.start({ name: 'drag' },
        event.interactable,
        dummy);
      }
    });
  
    //Dropzones
    interact('.dropzone').dropzone({
      // CSS selector that is needed for droppable element
      accept: '#dropObj',
      // minimum overlap for drop
      overlap: 0.4,
    
      //EVENTS
      ondropactivate: function (event) {},     //start dragging the object - migt be used to change style of canvas or show a "DROP HERE" text
      ondragenter: function (event) {

      },         //enter the dropzone
      ondragleave: function (event) {},         //leave the dropzone
      ondrop: function (event) {              //drop object in dropzone
        console.log('dropped');
        var dropObject = document.getElementById('dropObj');
        createCircleObject(); //alte createObject Methode, da Koordinatensystem noch nicht ganz klar
      },
      ondropdeactivate: function (event) {    //stop dragging the object (e.g.: drop somewhere else)
        var dropObject = document.getElementById('dropObj');
        dropObject.parentNode.removeChild(dropObject);  //delete the dummy object
      }
    });

 
  // Register Listener
  //document.getElementById('playButton').addEventListener('click', null);
  document.getElementById('createRectangle').addEventListener('click', createRectangleObject);
  document.getElementById('createCircle').addEventListener('click', createCircleObject);
  document.getElementById('createText').addEventListener('click', createTextObject);
  //document.getElementById('deleteButton').addEventListener('click', deleteSelectedCanvasObjects);

  document.getElementById('canvas').addEventListener('click', selectCanvasObject);
  document.addEventListener('keydown', shortcuts);
}

/*
  Helper functions
*/
function getCanvasObjects() {
  return document.getElementsByClassName('canvasObj');
}

function getSelectedCanvasObjects() {
  return document.getElementsByClassName('selectedObj');
}

/*
  Object related functions
*/
function createRectangleObject(type) {
  var aDiv = document.createElement('div');
  var divText = document.createTextNode('div-' + numberId);
  aDiv.appendChild(divText);
  aDiv.classList.add('rectangle');
  aDiv.classList.add('canvasObj');
  aDiv.classList.add('draggableObj');
  aDiv.setAttribute('id', 'div-' + numberId);

  canvasObjects.push(aDiv);

  var canvas = document.getElementById('canvas');
  canvas.appendChild(aDiv);

  numberId++;
  updatePropertiesTab(1);


}

function createCircleObject(type) {
  var aDiv = document.createElement('div');
  var divText = document.createTextNode('div-' + numberId);
  aDiv.appendChild(divText);
  aDiv.classList.add('circle');
  aDiv.classList.add('canvasObj');
  aDiv.classList.add('draggableObj');
  aDiv.setAttribute('id', 'div-' + numberId);
  aDiv.style.backgroundColor = 'red';
  canvasObjects.push(aDiv);

  var canvas = document.getElementById('canvas');
  canvas.appendChild(aDiv);

  numberId++;
  updatePropertiesTab(1);

}

function createTextObject(type) {
  var aDiv = document.createElement('div');
  var divText = document.createTextNode('Text hier!');
  aDiv.appendChild(divText);
  aDiv.classList.add('textarea');
  aDiv.classList.add('canvasObj');
  aDiv.classList.add('draggableObj');
  aDiv.setAttribute('id', 'div-' + numberId);
  aDiv.setAttribute('contenteditable', 'true');

  canvasObjects.push(aDiv);

  var canvas = document.getElementById('canvas');
  canvas.appendChild(aDiv);

  numberId++;
  updatePropertiesTab(1);
}


function selectCanvasObject(event) {
  var strgDown = event.ctrlKey;
  var isObjectInCanvas = event.target.classList.contains('canvasObj');
  var isObjectAlreadySelected = event.target.classList.contains('selectedObj');
  

  if (isObjectInCanvas && strgDown && !isObjectAlreadySelected) {

    // Ist ein Object im Canvas, Strg ist gedrückt und Object ist noch nicht selektiert
    event.target.classList.add('selectedObj');
    updatePropertiesTab(2, event.target);

  } else if (isObjectInCanvas && strgDown && isObjectAlreadySelected) {

    // Ist ein Object im Canvas, Strg ist gedrückt und Object ist bereits selektiert
    deselectCanvasObject(event.target);
    updatePropertiesTab(3);

  } else if (isObjectInCanvas) {

    // Ist ein Object im Canvas, Strg ist nicht gedrückt und Object ist noch nicht selektiert
    deselectAllCanvasObjects();
    event.target.classList.add('selectedObj');
    showProperties(event.target);

  } else {

    // Ins Canvas geklickt
    deselectAllCanvasObjects();
    resetElementsList();
  }
}

function deselectCanvasObject(target) {
  var targetId = target.id;
  console.log(targetId);
  document.getElementById(targetId).classList.remove('selectedObj');
}

function deselectAllCanvasObjects() {
  var elements = getSelectedCanvasObjects();

  while (elements.length > 0) {
    elements[0].classList.remove('selectedObj');
  }
}

function deleteSelectedCanvasObjects() {
  var canvas = document.getElementById('canvas');
  var objects = getSelectedCanvasObjects();

  while (objects.length > 0) {
    console.log(objects[0].id);
    canvas.removeChild(objects[0]);
  }
  updatePropertiesTab(3);
}


/*
  Listener functions
*/
function dragMoveListener(event) {
  var target = event.target,
    // keep the dragged position in the data-x/data-y attributes
    x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
    y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

  // translate the element
  target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';

  // update the posiion attributes
  target.setAttribute('data-x', x);
  target.setAttribute('data-y', y);
}

function shortcuts(event) {
  var strgDown = event.ctrlKey;
  var keyDown = event.key;

  if (strgDown && keyDown === 'a') {
    event.preventDefault();
    deselectAllCanvasObjects();

    var objs = getCanvasObjects();
    for (var i = 0; i < objs.length; i++) {
      objs[i].classList.add('selectedObj');
    }
  } else if (keyDown === 'Escape') {
    event.preventDefault();
    deselectAllCanvasObjects();
  } else if (keyDown === 'Delete') {
    event.preventDefault();
    deleteSelectedCanvasObjects();
  }
}



/* Ab hier Chris Code */


/* Ab hier Niklas Code */


/* Ab hier Michi Code */


/* Ab hier Philip Code */

/* Ab hier Marvin Code */

//MARVIN CODE ENDE
