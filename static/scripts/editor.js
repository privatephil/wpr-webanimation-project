/*
onload and window related functions
*/
$(document).ready(function() {
  setGUI_Layout();
  settings_Tabs();

  // var instance = M.Tabs.init(el, options);   ==  Initializes tabs without JQuery
  $('.tabs').tabs({
    duration: 100,
    swipeable: false,
  });

  ekUpload();

  // Category Select Initialisation
  var elems = document.querySelectorAll('select');
  var instances = M.FormSelect.init(elems, {});

  // LoadElements
  LoadAllElements();
  LoadIcons();

  // Show Category Items
  ShowSelectedCategory(document.getElementById('category'));
  // ShowSelectedCategory(document.getElementById('icon-category'));

  document.addEventListener('fullscreenchange', onFullScreenChange);
  document.addEventListener('webkitfullscreenchange', onFullScreenChange);
  document.addEventListener('mozfullscreenchange', onFullScreenChange);
  document.addEventListener('msfullscreenchange', onFullScreenChange);
  setGUI_Layout();
  console.log('ende onload editor.js');
});

window.onresize = function(event) {
  setGUI_Layout();
}

/*
GUI related functionality
*/
function settings_Tabs() {

  // Variables
  var clickedTab = $(".tabx > .active");
  var tabWrapper = $("#settings");
  var activeTab = tabWrapper.find(".active");

  // Show tab on page load
  activeTab.show();

  $(".tabx > li").on("click", function() {
    
    // Don't Fade-in IF Tab already active
    if(!$(this).hasClass('active')){

      // Remove class from active tab
      $(".tabx > li").removeClass("active");

      // Add class active to clicked tab
      $(this).addClass("active");

      // Update clickedTab variable
      clickedTab = $(".tabx .active");

      
        // fade out active tab
        activeTab.fadeOut(150, function() {
    
          // Remove active class all tabs
          $("#settings > div").removeClass("active");
    
          // Get index of clicked tab
          var clickedTabIndex = clickedTab.index();
          // console.log(clickedTabIndex);
    
    
          // Add class active to corresponding tab
          $("#settings > div").eq(clickedTabIndex).addClass("active");
    
          // update new active tab
          activeTab = $("#settings > .active");
    
          // Update variable
    
    
          // Animate height of wrapper to new tab height
          tabWrapper.stop().delay(0).animate({
            opacity: 100
          }, 150, function() {
    
            // Fade in active tab
            activeTab.fadeIn(150);
    
          });
        });
      }


    });


};

// Kann noch optimiert werden für die Performance
// Instanzvariabeln könnten z.B Global sein und nicht jedes mal neu initiiert werden
function setGUI_Layout() {

  var ww = window.innerWidth;
  var wh = window.innerHeight;

  // Format Options
  let canvas_ratio = 16 / 9;
  let container_division = 2 / 3;

  // Fixed Height Settings
  var margin = 16;
  let controls_height = 50;
  let nav_height = 64;
  let timeline_height = 100;
  let spacings_sum = nav_height + 3 * margin + controls_height + timeline_height;

  let container_height = (1 / canvas_ratio) * container_division * (ww - (2 * margin));
  let canvas_height = container_height;
  let canvas_width = canvas_height * canvas_ratio;
  let container_width = canvas_width * 1.5;

  // For Realive Font size
  let ch = $('#canvas').height();
  // console.log(ch);
  $('#canvas').css({
    'font-size': ch + 'px',
  });
  

  let fullscreenDisabled = true;

  if(document.fullscreenElement){
    fullscreenDisabled = !document.fullscreenElement;
  }
  else if(document.webkitFullscreenElement){
    fullscreenDisabled = !document.webkitFullscreenElement;
  }
  else if(document.mozFullScreenElement){
    fullscreenDisabled = !document.mozFullScreenEnabled;
  }
  else if(document.webkitCurrentFullScreenElement){
    fullscreenDisabled = !document.webkitCurrentFullScreenElement;
  }


  if(fullscreenDisabled){

    if (container_height + spacings_sum > wh) {
      scale_factor = (wh - (spacings_sum)) / container_height;
      canvas_height = canvas_height * scale_factor;
      canvas_width = canvas_width * scale_factor;
      container_width = container_width * scale_factor;
      container_height = container_height * scale_factor;
    }
    container_height = container_height + controls_height;

    $('.nav-wrapper').css({
      'width': container_width + 'px',
    });

    $('.nav-icon li').css({
      'width': (container_width - canvas_width - margin)/3*0.5 + 'px',
    });
    $('.nav-icon:nth-child(-n+5) li a i').css({
      'font-size': '20px',
    });

    $('#container').css({
      'height': container_height + 'px',
      'width': container_width + 'px',
      'margin-top': margin + 'px'
    });

  // Canvas Container
    $('#canvas-container').css({
      'height': canvas_height + 'px',
      'width': canvas_width + 'px'
    });

        $('#canvas').css({
          'height': canvas_height + 'px',
          'width': canvas_width + 'px',
          // 'font-size': canvas_height + 'px',
        });

  // Settings Container
    $('#settings-container').css({
      'width': container_width - canvas_width - margin + 'px',
      'height': container_height + 'px',
      'margin-left': margin + 'px'
    });
        $('#settings').css({
          'height': container_height - 50 + 'px'
        });
            $('#elements').css({
              'height': container_height - 50 + 'px'
            });
                $('#shapes-container').css({
                  'height': container_height - 50 - 48 + 'px'
                });
                $('#text-container').css({
                  'height': container_height - 50 - 48 +'px'
                });
                $('#pictures-container').css({
                  'height': container_height - 50 - 48 + 'px'
                });


            $('#adjustments').css({
              'height': container_height - 50 + 'px'
            });


    // Timeline
    $('#timeline').css({
      'margin-top': margin + 'px'
    });

  }
};


/* Ab hier Niklas Code */
// File Upload
function ekUpload(){
  function Init() {

    // console.log("Upload Initialised");
    var fileSelect    = document.getElementById('file-upload'),
        fileDrag      = document.getElementById('file-drag'),
        submitButton  = document.getElementById('submit-button');

    fileSelect.addEventListener('change', fileSelectHandler, false);

    // Is XHR2 available?
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
      // File Drop
      fileDrag.addEventListener('dragover', fileDragHover, false);
      fileDrag.addEventListener('dragleave', fileDragHover, false);
      fileDrag.addEventListener('drop', fileSelectHandler, false);
    }
  }

  function fileDragHover(e) {
    var fileDrag = document.getElementById('file-drag');
    e.stopPropagation();
    e.preventDefault();
    fileDrag.className = (e.type === 'dragover' ? 'hover-upload' : '');
  }

  function fileSelectHandler(e) {
    // Fetch FileList object
    var files = e.target.files || e.dataTransfer.files;
    
    // Cancel event and hover styling
    fileDragHover(e);

    // ONLY Upload IMAGES !
    for (var i = 0, f; f = files[i]; i++) {
      var isGood = (/\.(?=gif|jpg|png|jpeg|svg)/gi).test(f.name);
      if(isGood == false ){
        files.splice(i,1);
      }
    }
    
    // Process all File objects
    for (var i = 0, f; f = files[i]; i++) {
      parseFile(f);
      uploadFile(f);
    }
  }

  // Output
  function output(msg) {
    // Response
    var m = document.getElementById('messages');
    m.innerHTML = msg;
  }

  function parseFile(file) {

    output(
      '<strong>' + encodeURI(file.name) + '</strong>'
    );
    
    // var fileType = file.type;
    // console.log(fileType);
    var imageName = file.name;

    var isGood = (/\.(?=gif|jpg|png|jpeg|svg)/gi).test(imageName);
    if (isGood) {
      document.getElementById('start').classList.add("hidden");
      document.getElementById('response').classList.remove("hidden");
      document.getElementById('notimage').classList.add("hidden");
      // Thumbnail Preview
      document.getElementById('file-image').classList.remove("hidden");
      document.getElementById('file-image').src = URL.createObjectURL(file);
    }
    else {
      document.getElementById('file-image').classList.add("hidden");
      document.getElementById('notimage').classList.remove("hidden");
      document.getElementById('start').classList.remove("hidden");
      document.getElementById('response').classList.add("hidden");
      document.getElementById("file-upload-form").reset();
    }
  }

  function setProgressMaxValue(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.max = e.total;
    }
  }

  function updateFileProgress(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.value = e.loaded;
    }
  }

  function uploadFile(file) {

    var xhr = new XMLHttpRequest(),
      pBar = document.getElementById('file-progress'),
      fileSizeLimit = 10; // In MB
    if (xhr.upload) {
      // Check if file is less than x MB
      if (file.size <= fileSizeLimit * 1024 * 1024) {
        // Progress bar
        pBar.style.display = 'inline';
        xhr.upload.addEventListener('loadstart', setProgressMaxValue, false);
        xhr.upload.addEventListener('progress', updateFileProgress, false);

        // File received / failed
        xhr.onreadystatechange = function(e) {
          if (xhr.readyState == 4) {
            // Everything is good!

            // progress.className = (xhr.status == 200 ? "success" : "failure");
            // document.location.reload(true);
            // Reset Upload Form
            setTimeout(function(){ 
                document.getElementById('file-image').classList.add("hidden");
                document.getElementById('notimage').classList.remove("hidden");
                document.getElementById('start').classList.remove("hidden");
                document.getElementById('response').classList.add("hidden");
                document.getElementById("file-upload-form").reset();
            }, 3000);
            // Refresh Elements After Upload
            LoadAllElements();
            console.log('Upload done');
          }
        };

        var formData = new FormData();
        formData.append("fileUpload", file);
        formData.append("category", document.getElementById('category').value);
        // Start upload
        xhr.open('POST', document.getElementById('file-upload-form').action, true);
        xhr.setRequestHeader('X-File-Name', file.name);
        xhr.setRequestHeader('X-File-Size', file.size);
        xhr.send(formData);
        
      } else {
        output('Please upload a smaller file (< ' + fileSizeLimit + ' MB).');
      }
    }
  }

  // Check for the various File API support.
  if (window.File && window.FileList && window.FileReader) {
    Init();
  } else {
    document.getElementById('file-drag').style.display = 'none';
  }
}


function ShowSelectedCategory(Object){ 
  // fadeout all inactive containers
  let children = Object.children;
  let i;
  for (i = 0; i < children.length; i++) {
    let disabledContainer = document.getElementById(children[i].value);
    if(disabledContainer){
      
      // 
      if(children[i].value !== Object.value){
        disabledContainer.classList.toggle("fade-out", true);
        document.getElementById('element-upload').classList.toggle("fade-out", true);
        setTimeout(function(){disabledContainer.classList.toggle("display-none", true)},310);
      }
      
    }

  }

  // fadeIn all Active containers
  // console.log(Object.value);
  let activeContainer = document.getElementById(Object.value);
  
  setTimeout(function(){
      activeContainer.classList.toggle("display-none", false);
      activeContainer.classList.toggle("fade-out", false);
      document.getElementById('element-upload').classList.toggle("fade-out", false);
  },350);

  }


  function deleteElement(element){
    
      element.parentElement.parentElement.classList.add("slide-out");
      var id = element.parentElement.parentElement.getAttribute('data-elementid');
      setTimeout(function(){
        element.parentElement.parentElement.remove();
      }, 1000);
      

      var xhr = new XMLHttpRequest();
      xhr.open('DELETE', '/elements/shapes', true);
      xhr.setRequestHeader('id', id);
      xhr.send();
      
  }

function createSquareIMG(element, callback){


  var ele = document.createElement('div');
  ele.classList.add('square');
  ele.classList.add('dark-grey-3');
  ele.setAttribute('data-elementid', element['ObjectId'])

  ele.innerHTML = ` <div class="square-content">
                        <img src='${element['ObjectFile']}' class='fit-in dragDropObj'>
                        <div class='delete-element valign-wrapper' onclick='deleteElement(this)'>
                          <img src='/files/SVG/baseline-close-24px.svg'>
                        </div>
                    </div>`;

  let creator = ele.childNodes[1].childNodes[1];
  if(creator.getAttribute('src').endsWith('.svg')) {
    console.log("Is an svg");
    creator.classList.add('shapecreator');  //later replace with Vectorcreator!(or similar)
  }
  else{
    console.log("is no svg");
    creator.classList.add('shapecreator');
  }
  callback(ele);
  
}

function createSquareIcon(filePath, callback){

  var ele = document.createElement('div');
  ele.classList.add('square');
  ele.classList.add('dark-grey-3');

  ele.innerHTML = ` <div class="square-content">
                        <img src='${filePath}' class='fit-in dragDropObj'>
                    </div>`;

  let creator = ele.childNodes[1].childNodes[1];
  if(creator.getAttribute('src').endsWith('.svg')) {
    console.log("Is an svg");
    creator.classList.add('shapecreator');  //later replace with Vectorcreator!(or similar)
  }
  else{
    console.log("is no svg");
    creator.classList.add('shapecreator');
  }
  callback(ele);
  
}

function LoadAllElements(){
    var category = document.getElementById('category');
    var i;
    for(i=0; i<category.children.length; i++ ){
        loadElements(category.children[i]);
    }
}


function loadElements(Object){
  var xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) { 
      var myArr = JSON.parse(this.responseText);
      insertElements(myArr);
    }
  };


  function insertElements(arr) {
    var elm = document.getElementById(Object.value);
 
    // Remove all Elements from container
    while (elm.firstChild) {
      elm.removeChild(elm.firstChild);
    }

    // Add all Elements to container
    arr.forEach(element => {
      createSquareIMG(element, function(ele){
        elm.appendChild(ele);
    })
    });

  }

  xhr.open('GET', '/elements/shapes', true);
  xhr.setRequestHeader('category', Object.value);
  xhr.send();
  // console.log('whabulabudabdab');

}

function LoadIcons(Object){
  var xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) { 
      var myArr = JSON.parse(this.responseText);
      console.log(myArr)
      insertElements(myArr);
    }
  };

  function insertElements(folders) {
    var elm = document.getElementById('icons-category-container');
    var categ = document.getElementById('icon-category');

    // Remove all Elements from container
    while (elm.firstChild) {
      elm.removeChild(elm.firstChild);
    }

    // Add all Elements to container
    folders.forEach((folder, index) => {
      let divFolder = document.createElement('div')
      divFolder.classList = "row margin-0 padding-0 dark-grey-4 fade-out display-none"
      // if Folder ID is availabe
      if(!document.getElementById(folder.name)){
        let id = folder.name
        divFolder.id = id
        let option = document.createElement('option')
        option.value = id
        option.innerHTML = id
        categ.appendChild(option)
      }
      // if Folder ID is already taken
      else{
        let id = folder.name + 'copy'
        divFolder.id = id
        let option = document.createElement('option')
        option.value = id
        option.innerHTML = id
        categ.appendChild(option)
      }
      // Create Category(Folder) Container
      elm.appendChild(divFolder);

      // Add Images to Category(Folder) Container
      folder.paths.forEach((path) => {
        createSquareIcon(path, function(ele){
          divFolder.appendChild(ele);
        })
      })
    });
    // Initialize Dropdown again -> Update dropdowns
    categ.selectedIndex = '0';
    elems = document.querySelectorAll('select');
    M.FormSelect.init(elems, {});
    // Show the selected containergg
    ShowSelectedCategory(categ)
  }

  xhr.open('GET', '/elements/icons', true);
  xhr.send();

}

/* Ab hier Chris Code */
function onFullScreenChange(e)
  {
    let canvas = getCanvas();
    let controls = document.getElementById('controls');
    let nav = document.getElementById('fullscreenNav');
    let navB = document.getElementById('showNavButton');
    if(document.fullscreenElement)  // || document.fullscreenEnabled|| document.mozFullScreen
    {
      canvas.style.width = '100vw';
      canvas.style.height = '56.25vw';
      canvas.style.pointerEvents = 'none';
      controls.style.display = 'none';
      nav.style.display = 'block';
      navB.style.display = 'block';
      setGUI_Layout();
    }
    else{
      canvas.style.pointerEvents = 'auto';
      setGUI_Layout();
      controls.style.display = 'block';
      nav.style.display = 'none';
      navB.style.display = 'none';
    }
  }


function goFullScreen(){
  var canvas = document.getElementById("canvas-container");

  if(!document.fullscreenElement){
    if(canvas.requestFullScreen)
        canvas.requestFullScreen();
    else if(canvas.webkitRequestFullScreen)
        canvas.webkitRequestFullScreen();
    else if(canvas.mozRequestFullScreen)
        canvas.mozRequestFullScreen();
  }
  else{
    if(document.exitFullscreen)
       document.exitFullscreen();
    else if(document.webkitExitFullscreen)
        document.webkitExitFullscreen();
    else if(document.mozCancelFullScreen)
        document.mozCancelFullScreen();
  }
}

/* Ab hier Michi Code */


/* Ab hier Philipp Code */