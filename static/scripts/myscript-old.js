var keyframes = [];
var count = 2;

window.onload = function() {

  meldeErstePosition(document.getElementById('resize-drag'));

  interact('.resize-drag')
    .draggable({
      onmove: window.dragMoveListener,
      modifiers: [
        interact.modifiers.restrict({
          restriction: 'parent',
          elementRect: {
            top: 0,
            left: 0,
            bottom: 1,
            right: 1
          }
        })
      ]
    }).on('dragend', dragStopListener);

};

function dragMoveListener(event) {
  var target = event.target,
    // keep the dragged position in the data-x/data-y attributes
    x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
    y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

  // translate the element
  target.style.webkitTransform =
    target.style.transform =
    'translate(' + x + 'px, ' + y + 'px)';

  // update the posiion attributes
  target.setAttribute('data-x', x);
  target.setAttribute('data-y', y);
}


function dragStopListener(event) {

  var target = event.target,
  x = (parseFloat(target.getAttribute('data-x')) || 0),
  y = (parseFloat(target.getAttribute('data-y')) || 0);

  keyframes.push({translateX: x, translateY: y});

  // console.log(keyframes);
}

function meldeErstePosition(myElement) {
  keyframes.push({translateX: 0, translateY: 0});
}

function createKeyframe() {
  var value = document.getElementById('myRange').value;
  var myDiv = document.createElement('div');

  myDiv.setAttribute('id', 'punkt' + count);
  count++;

  document.getElementById('timeline').appendChild(myDiv);

  console.log(value);
}

function play() {
  anime({
    targets: '.resize-drag',
    keyframes,
    duration: 10000,
    easing: 'easeInOutQuad',
    loop: false
  });
}
// this is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener;
