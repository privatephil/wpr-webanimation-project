let shapeCount = 1;
let keyframeCount = 1;

let wholeDuration = document.getElementById('timespan').value * 1000;
let bgColor = '#FFFFFF';

function getCanvas() {
	return document.getElementById('canvas');
}

function getKeyframeArea() {
	return document.getElementById('keyframeArea');
}

//Compare-Function for sortKeyframes()
function compareRelativeTimeValue(a, b) {
	return a.relativeTimeValue - b.relativeTimeValue;
}

// Converts from degrees to radians.
Math.radians = function (degrees) {
	return degrees * Math.PI / 180;
};

class AnimationPlayer {
	constructor(shapesList) {
		this.progress = document.getElementById('timelinePointer');

		this.isLoop = true;
		this.duration = wholeDuration;
		this.easying = 'easeInOutSine'; // easeOutElastic(1, .8)

		this.shapesList = shapesList;
	}

	pushKeyframe(keyframe) {
		this.keyframes.push(keyframe);
	}

	setAnimations() {
		if (this.shapesList) {
			this.shapesList.getAll().forEach(shape => {
				shape.setAnimation(this.duration, this.isLoop, this.easying, this.progress);
			});
		}
		this.setProgress();
	}

	play() {
		if (this.shapesList) {
			this.shapesList.getAll().forEach(shape => {
				shape.animation.play();
			});
		}

	}

	pause() {
		if (this.shapesList) {
			this.shapesList.getAll().forEach(shape => {
				shape.getAnimation().pause();
			});
		}
	}

	gotoTime(time) {
		if (this.shapesList) {
			this.shapesList.getAll().forEach(shape => {
				shape.getAnimation().seek(time / 100 * this.duration);
			});
		}
	}

	setProgress() {
		// console.log(this.progress.value);

		if (this.shapesList) {
			this.shapesList.getAll().forEach(shape => {
				shape.getAnimation().seek((this.progress.value / 100 * this.duration));
			});
		}
	}
}

/* List of Keyframes */
class KeyframesManager {

	constructor(shapesList) {
		this.shapesList = shapesList;
	}

	getKeyframe(htmlId) {
		let selectedShapes = this.shapesList.getSelected();
		let foundKeyframe;

		selectedShapes.forEach(shape => {
			let keyframes = shape.getKeyframes();

			keyframes.forEach(kf => {
				if (kf.htmlId === htmlId) {
					console.log("found keyframe!");
					foundKeyframe = kf;
				}
			});
		});

		return foundKeyframe;
	}

	hideAllKeyframes() {
		this.keyframes.forEach(el => el.hide());
	}

	getSelected() {
		let list = []
		let selectedShapes = this.shapesList.getSelected();

		selectedShapes.forEach(shape => {
			let keyframes = shape.getKeyframes();

			keyframes.forEach(kf => {
				if (kf.selected) {
					list.push(kf);
				}
			});
		});

		return list;
	}

	deselectAll() {
		let selectedShapes = this.shapesList.getSelected();

		selectedShapes.forEach(shape => {
			let keyframes = shape.getKeyframes();

			keyframes.forEach(kf => {
				kf.deselect();
			});
		});
	}

	deleteKeyframes() {
		let selectedShapes = this.shapesList.getSelected();
		let selectedKeyframes = this.getSelected();

		selectedShapes.forEach(shape => {
			shape.deleteKeyframes(selectedKeyframes);
		});
	}

}


/* A keyframe instance */
class Keyframe {
	constructor(valueOfTimelinePointer, values, isFirst, isLast) {
		this.kfId = keyframeCount++;
		this.isFirst = isFirst;
		this.isLast = isLast;
		this.relativeTimeValue = parseFloat(valueOfTimelinePointer);

		this.htmlId = 'keyframe-' + this.kfId; //id of liveHTMLObject

		this.selected = false;
		if (isFirst || isLast) {
			this.hidden = true;
		}
		else {
			this.hidden = false;
		}
		this.element = this.create(valueOfTimelinePointer, (isFirst || isLast)); //liveHTMLObject

		this.values = values; //the changes (packed as json object) the keyframe invokes on the associated shape
	}

	create(valueOfTimelinePointer, isBoundaryKeyframe) {
		let element = document.createElement('div');
		element.id = this.htmlId;

		if (!isBoundaryKeyframe) element.classList.add('keyframe');
		else element.classList.add('boundaryKeyframe');
		// position the keyframe inside the keyframe area
		if(getKeyframeArea()){
			getKeyframeArea().appendChild(element);
			let keyframeBounds = element.getBoundingClientRect();
			let keyframeAreaBounds = document.getElementById('keyframeArea').getBoundingClientRect();
			
			// console.log(keyframeAreaBounds.width);
			// console.log(keyframeBounds.width);
			
			element.style.left = valueOfTimelinePointer - (valueOfTimelinePointer * keyframeBounds.width / keyframeAreaBounds.width) + '%';
			element.setAttribute('data-x', valueOfTimelinePointer)
		}

		return element;
	}

	setRelativeTimeValue(relativeTime) {
		if (relativeTime) {
			this.relativeTimeValue = parseFloat(relativeTime);
		}
	}

	getRelativeTimeValue() {
		return this.relativeTimeValue;
	}

	changeValueOfTimeLinePointer(dxStart, dxEnd) {
		let diff = dxEnd - dxStart;

		if (diff > 0) {

		} else {

		}

		console.log(this.relativeTimeValue);
	}

	remove() {
		this.element.parentNode.removeChild(this.element);
	}

	isSelected() {
		return this.selected;
	}

	select() {
		this.selected = true;

		this.element.classList.add('selectedKeyframe');
	}

	deselect() {
		this.selected = false;

		this.element.classList.remove('selectedKeyframe');
	}

	hide() {
		this.hidden = true;

		this.element.classList.add('hidden');

		this.deselect();
	}

	show() {
		this.hidden = false;

		this.element.classList.remove('hidden');
	}

	getLiveHTMLElement() {
		return this.element;
	}

	getValues() {
		return this.values;
	}

	updateValues(selectedElement) {
		this.values.position.x = parseFloat(selectedElement.style.left);
		this.values.position.y = parseFloat(selectedElement.style.top);
		this.values.scale.x = parseFloat(selectedElement.style.width);
		this.values.scale.y = parseFloat(selectedElement.style.height);

		//get Element's Rotation
		let regex = new RegExp('rotate.([+-]?([0-9]*[.])?[0-9]+)rad');
		let res = regex.exec(selectedElement.style.transform);
		if(res) {this.values.rotation = parseFloat(res[1]);}
		else {this.values.rotation = 0;}

		this.values.opacity = parseFloat(selectedElement.style.opacity);
	}
}

/* List of shapes placed in canvas */
class ShapeList {
	constructor() {
		this.shapeList = [];
		this.wholeDuration = wholeDuration;
		this.backgroundColor = bgColor;
	}

	/* Adds the shape to the list */
	addShape(shape) {
		this.shapeList.push(shape);
	}

	/* Removes the shape from the list */
	removeShape(shape) {
		console.log(shape);
		shape.hideKeyframes();
		for (let i = 0; i < this.shapeList.length; i++) {
			if (this.shapeList[i].htmlId === shape.htmlId) {
				shape.remove();
				this.shapeList.splice(i, 1);
			}
		}
	}

	/* Removes multiple shapes from the list */
	removeShapes(shapes) {
		console.log("Shapes deleted:");
		let c = 0;
		for (let i = shapes.length; i > 0; i--) {
			this.removeShape(shapes[c++]);
		}

		return shapes;
	}

	/* Returns every shape from the list and therefore from canvas */
	getAll() {
		return this.shapeList;
	}

	getShape(htmlId) {
		let foundShape;
		this.shapeList.forEach(shape => {
			if (shape.htmlId === htmlId) {
				foundShape = shape;
			}
		});

		return foundShape;
	}

	/* Returns every selected shape from the list */
	getSelected() {
		let list = []
		this.shapeList.forEach(shape => {
			if (shape.isSelected()) {
				list.push(shape);
			}
		});

		return list;
	}

	// Returns the Bounds of all selected Elements
	getSelectedBounds() {
		let selectedShapes = this.getSelected();
		let canvas = getCanvas().getBoundingClientRect();

		var bounds = {
			xmin: 10000,
			xmax: 0,
			ymin: 10000,
			ymax: 0,
			width: 0,
			height: 0,
			left: 0,
			top: 0,
		}

		// Calc minimal bounds for all selected Objects
		for (var i = 0; i < selectedShapes.length; i++) {
			var s = selectedShapes[i].element.getBoundingClientRect();
			if (s) {
				if (s.left < bounds.xmin) {
					bounds.xmin = s.left;
				}
				if (s.right > bounds.xmax) {
					bounds.xmax = s.right;
				}
				if (s.top < bounds.ymin) {
					bounds.ymin = s.top;
				}
				if (s.bottom > bounds.ymax) {
					bounds.ymax = s.bottom;
				}
			}
		}
		// Calc in Pixels
		bounds.width = bounds.xmax - bounds.xmin;
		bounds.height = bounds.ymax - bounds.ymin;
		bounds.left = bounds.xmin - canvas.left + (bounds.width / 2);
		bounds.top = bounds.ymin - canvas.top + (bounds.height / 2);
		return bounds;
	}

	/* Selects all shapes in the list and therefore on canvas */
	selectAll() {
		this.shapeList.forEach(shape => {
			shape.select();
		});
	}

	/* Deselects all shapes in the list and therefore on canvas */
	deselectAll() {
		this.shapeList.forEach(shape => {
			shape.deselect();
		});
	}

	/* Returns the shape associated with the given keyframe */
	getShapeFromKeyframe(keyframe) {

	}

	loadObj(obj) {
		// console.log('loadObj in ShapeList');

		//wrap the element
		let div = document.createElement('div');
		div.innerHTML = obj.htmlText.trim();

		// console.log(div.firstChild);

		let shape;
		if(div.firstChild.classList.contains('textField')) {
			console.log('Is textfield!');
			// console.log(div.firstChild);
			shape = new TextField(div.firstChild, obj.htmlId);
		}
		else if(div.firstChild.classList.contains('vectorgraphic')) {
			console.log('Is vectorgraphic!');
			shape = new Vectorgraphic(div.firstChild, obj.htmlId);
		}
		else {
			console.log('Is a shape');
			shape = new Shape(div.firstChild, obj.htmlId);

		}

		shape.loadObjData(obj);

		this.shapeList.push(shape);
	}

	updateShapeIds() {
		let highest = 0;
		let id;
		this.shapeList.forEach(shape => {
			// htmlId has form shape-{id}
			id = shape.htmlId;
			id = parseInt(id.match(/shape\-([0-9]*)/)[1]);

			if(id > highest) {
				highest = id;
			}
		});

		shapeCount = highest + 1;
	}

	setHtmlTextsForShapes() {
		this.shapeList.forEach(shape => {
			var target = shape.element;
			var wrap = document.createElement('div');
			wrap.appendChild(target.cloneNode(true));

			/* Needs to be done, because chrome (and maybe other browsers) create quotes around font declarations */
			shape.htmlText = wrap.innerHTML.replace(/&quot;/g, '');

			console.log(shape.htmlText);
		});

	}
}

/* A canvas shape */
class Shape {
	constructor(elementToClone, htmlId) {
		this.keyframes = [] //new LinkedList();
		if (htmlId) {
			this.htmlId = htmlId;
		} else {
			this.htmlId = 'shape-' + shapeCount++; //id of liveHTMLObject
		}
		this.element = this.create(elementToClone); //liveHTMLObject
		this.element.setAttribute('data-x', 10);
		this.element.setAttribute('data-y', 10);
		this.element.style.left = 10 + '%';
		this.element.style.top = 10 + '%';
		this.element.style.opacity = '1';
		this.element.style.zIndex = '1';
		this.element.style.width = 20 * (9 / 16) * (elementToClone.naturalWidth / elementToClone.naturalHeight) + '%';

		this.element.style.height = '20%';

		this.selected = false;
		this.animation = null;
		this.zIndex = parseFloat(this.element.style.zIndex);
		this.htmlText;

		this.element.setAttribute('preserveaspectratio', 'none');
		this.element.style['transform-origin'] = 'center center';
		this.element.style['transform'] = 'translate(-50%,-50%)';


		this.file;
		if (elementToClone.getAttribute('src')) {
			this.file = elementToClone.getAttribute('src');
		}





		//create a keyframe at the beginning and at the end to make sure the animation is always the same length
		//set the values
		let valuesOfBoundaryFrames = {
			position: {
				x: parseFloat(this.element.style.left),
				y: parseFloat(this.element.style.top)
			},

			scale: {
				x: (this.element.style.width),
				y: (this.element.style.height)
			},

			rotation: parseFloat(this.element.getAttribute('data-angle')) || 0,

			opacity: parseFloat(this.element.style.opacity)
		};
		this.createKeyframe(0, valuesOfBoundaryFrames, true, false);
		this.createKeyframe(100.1, valuesOfBoundaryFrames, false, true);
	}

	loadObjData(obj) {
		this.htmlId = obj.htmlId;
		this.selected = obj.selected;
		this.file = obj.file;

		this.zIndex = obj.zIndex;
		document.getElementById(this.htmlId).style.zIndex = obj.zIndex;

		let keyframes = obj.keyframes;

		keyframes.forEach(kf => {
			// console.log('kf.values.rotation:' + kf.values.rotation);
			if (!kf.isFirst && !kf.isLast) this.createKeyframe(kf.relativeTimeValue, kf.values, kf.isFirst); //don't add the BoundaryFrames
		});
	}

	loadObjDataKeyframes() {

	}

	redraw(event) {
		console.log('Redraw the vector shape');

		let shiftKey = event.shiftKey;

		var target = this.element,
			x = (parseFloat(target.getAttribute('data-x')) || 0),
			y = (parseFloat(target.getAttribute('data-y')) || 0);


		var resizeRect = canvasPixelPositionToPercent(event.rect.width, event.rect.height);
		// update the element's style
		var width;
		var height;
		if (shiftKey) {
			let aspectRatio = (9 / 16) * (this.element.naturalWidth / this.element.naturalHeight);
			if (resizeRect.x > resizeRect.y) {
				width = resizeRect.x * aspectRatio + '%';
				height = resizeRect.x + '%';
			}
			else {
				width = resizeRect.y * aspectRatio + '%';
				height = resizeRect.y + '%';
			}
		}
		else {
			width = resizeRect.x + '%';
			height = resizeRect.y + '%';
		}

		event.target.style.width = width;
		event.target.style.height = height;
		target.style.width = width;
		target.style.height = height;

		updateValueInTab('width', parseFloat(width).toFixed(2));
		updateValueInTab('height', parseFloat(height).toFixed(2));
	}

	setAnimation(duration, isLoop, easing, progress) {
		let keyframes = [];
		let lastKfTimelineValue;

		this.keyframes.forEach(kf => {
			// console.log('kf position: ' + kf.getValues().position.x + ', ' + kf.getValues().position.y);
			// console.log('kf opacity: ' + kf.getValues().opacity);
			// console.log('kf width: ' + kf.getValues().scale.x);

			if (kf.isFirst) {
				keyframes.push(
					{
						left: kf.getValues().position.x,
						top: kf.getValues().position.y,
						width: kf.getValues().scale.x,
						height: kf.getValues().scale.y,
						// translateX: 0,
						// translateY: 0,
						duration: 0,
						opacity: kf.getValues().opacity,
						rotate: kf.getValues().rotation + 'rad',

					}
				);

				lastKfTimelineValue = kf.relativeTimeValue;
			} else {
				let t = ((kf.relativeTimeValue - lastKfTimelineValue) / 100) * wholeDuration;

				// console.log('kf.relativeTimeValue - lastKfTimelineValue = ' + (kf.relativeTimeValue - lastKfTimelineValue));
				// console.log('kf duration: ' + t);

				keyframes.push(
					{
						left: kf.getValues().position.x,
						top: kf.getValues().position.y,
						width: kf.getValues().scale.x,
						height: kf.getValues().scale.y,
						// translateX: 600,
						// translateY: 250,
						duration: t,
						opacity: kf.getValues().opacity,
						rotate: kf.getValues().rotation + 'rad',
					}
				);
				lastKfTimelineValue = kf.relativeTimeValue;
			}

		});

		// console.log(this.element.id);

		let t1 = anime({
			targets: '#' + this.element.id,
			keyframes: keyframes,
			duration: duration,
			loop: isLoop,
			autoplay: false,
			easing: easing,
			update: function (anim) {
				progress.value = t1.progress;
			}
		});

		// reset the event listener on progress
		progress.removeEventListener('input', function () {
			t1.seek(t1.duration * (progress.value / 100));
		});

		progress.addEventListener('input', function () {
			t1.seek(t1.duration * (progress.value / 100));
		});

		this.animation = t1;
	}

	getAnimation() {
		return this.animation;
	}

	/* Clones and sets the cloned element on the canvas */
	create(element) {
		let clonedElement = element.cloneNode(true); //false: does not clone inner child elements
		clonedElement.id = this.htmlId;
		clonedElement.className = '';
		clonedElement.classList.add('draggable');
		clonedElement.classList.add('shape');
		getCanvas().appendChild(clonedElement);

		return clonedElement;
	}

	/* Removes the liveHTMLObject from canvas */
	remove() {
		this.element.parentNode.removeChild(this.element);
	}

	/* Sets the liveHTMLObject as selected with class */
	select() {
		this.selected = true;
		this.element.classList.add('selected');

		this.showKeyframes();
	}

	/* Removes the selected class from liveHTMLObject */
	deselect() {
		this.selected = false;
		this.element.classList.remove('selected');

		this.hideKeyframes();
	}

	/* Returns the selected state of the liveHTMLObject */
	isSelected() {
		return this.selected;
	}

	addKeyframe(keyframe) {
		this.keyframes.push(keyframe);
	}

	createKeyframe(valueOfTimelinePointer, values, isFirst, isLast) {
		let keyframe = new Keyframe(valueOfTimelinePointer, values, isFirst, isLast);

		this.keyframes.push(keyframe);
		this.sortKeyframes();
	}

	deleteKeyframes(keyframesToDelete) {
		keyframesToDelete.forEach(kfToDelete => {
			for (let i = 0; i < this.keyframes.length; i++) {
				if (kfToDelete.htmlId === this.keyframes[i].htmlId) {
					this.keyframes[i].remove();
					this.keyframes.splice(i, 1);
				}
			}
		});
		this.sortKeyframes();
	}

	getKeyframe(htmlId) {
		let foundKeyframe;

		this.keyframes.forEach(kf => {
			if (kf.htmlId === htmlId) {
				foundKeyframe = kf;
			}
		});

		return foundKeyframe;
	}

	getKeyframes() {
		return this.keyframes;
	}

	showKeyframes() {
		this.keyframes.forEach(kf => {
			kf.show();
		});
	}

	hideKeyframes() {
		//console.log("hideKeyframes()");

		this.keyframes.forEach(kf => {
			kf.hide();
		});
	}


	sortKeyframes() {
		var keyframes = this.keyframes;
		var length = keyframes.length;
		if (keyframes != null && length > 1) {
			// console.log("SORTING!");
			//sort Keyframes by TimeValue
			keyframes.sort(compareRelativeTimeValue);
			//reArrange Keyframes IDs and "isFirst" Status
			//first and last Keyframes are handled separately
			//First:
			keyframes[0].kfId = 1;
			keyframes[0].isFirst = true;
			keyframes[0].isLast = false;
			//Last Element
			keyframes[length - 1].kfId = length;
			keyframes[length - 1].isFirst = false;
			keyframes[length - 1].islast = true;
			//and the Rest
			if (length > 2) {
				for (var i = 1; i < length - 1; i++) {
					keyframes[i].kfId = (i + 1);
					keyframes[i].isFirst = false;
					keyframes[i].isLast = false;
				}
				//copy values of first and last Real Keyframe to the Boundary Keyframes
				keyframes[0].values = keyframes[1].values;
				keyframes[length - 1].values = keyframes[length - 2].values;
			}
		}
		// player.setAnimations();

	}

	setZIndex(newZIndex) {
		this.zIndex = newZIndex;
	}
}
/*
class Rectangle extends Shape {
	constructor(elementToClone) {
		super(elementToClone);
		// this.element.classList.add('drawn-rectangle');

		this.element.width = 50;
		this.element.height = 50;

		let ctx = this.element.getContext("2d");
		ctx.beginPath();
		ctx.lineWidth = "2";
		ctx.strokeStyle = "black";
		ctx.rect(0, 0, 50, 50);
		ctx.stroke();
	}

	redraw(event) {
		console.log('Redraw the rectangle');

		var target = event.target,
			x = (parseFloat(target.getAttribute('data-x')) || 0),
			y = (parseFloat(target.getAttribute('data-y')) || 0);

		// update the element's style
		// target.style.width  = event.rect.width + 'px';
		// target.style.height = event.rect.height + 'px';
		// target.setAttribute('viewBox', '0 0 ' + event.rect.width + ' ' + event.rect.height);
		target.setAttribute('width', event.rect.width);
		target.setAttribute('height', event.rect.height);

		event.target.style.transform = 'scale(' + event.rect.width + ', ' + event.rect.height + ')';
		event.target.style.width = event.rect.width;
		event.target.style.height = event.rect.height;


		let ctx = event.target.getContext("2d");
		ctx.beginPath();
		ctx.lineWidth = "2";
		ctx.strokeStyle = "black";
		ctx.rect(0, 0, 50 * (event.rect.width / 50), 50 * (event.rect.height / 50));
		ctx.stroke();

		// translate when resizing from top or left edges
		x += event.deltaRect.left;
		y += event.deltaRect.top;

		target.style.webkitTransform = target.style.transform =
			'translate(' + x + 'px,' + y + 'px)';

		target.setAttribute('data-x', x);
		target.setAttribute('data-y', y);
	}
}

class Ellipse extends Shape {
	constructor(elementToClone) {
		super(elementToClone);
		// this.element.classList.add('drawn-rectangle');

		this.element.width = 100;
		this.element.height = 75;

		this.drawEllipse(parseInt(this.element.width) / 2, parseInt(this.element.height) / 2, 100 * (this.element.width / 100) - 2, 75 * (this.element.height / 75) - 2);
	}

	drawEllipse(centerX, centerY, width, height) {
		let ctx = this.element.getContext("2d");
		ctx.beginPath();

		ctx.moveTo(centerX, centerY - height / 2); // A1

		ctx.bezierCurveTo(
			centerX + width / 2, centerY - height / 2, // C1
			centerX + width / 2, centerY + height / 2, // C2
			centerX, centerY + height / 2); // A2

		ctx.bezierCurveTo(
			centerX - width / 2, centerY + height / 2, // C3
			centerX - width / 2, centerY - height / 2, // C4
			centerX, centerY - height / 2); // A1


		// ctx.fillStyle = "red";
		// ctx.fill();

		ctx.lineWidth = "2";
		ctx.strokeStyle = "black";
		ctx.stroke();
		ctx.closePath();
	}

	redraw(event) {
		console.log('Redraw the circle');

		var target = event.target,
			x = (parseFloat(target.getAttribute('data-x')) || 0),
			y = (parseFloat(target.getAttribute('data-y')) || 0);

		// update the element's style
		// target.style.width  = event.rect.width + 'px';
		// target.style.height = event.rect.height + 'px';
		// target.setAttribute('viewBox', '0 0 ' + event.rect.width + ' ' + event.rect.height);
		target.setAttribute('width', event.rect.width);
		target.setAttribute('height', event.rect.height);

		event.target.style.transform = 'scale(' + event.rect.width + ', ' + event.rect.height + ')';
		event.target.style.width = event.rect.width;
		event.target.style.height = event.rect.height;

		// translate when resizing from top or left edges
		x += event.deltaRect.left;
		y += event.deltaRect.top;

		this.drawEllipse(event.rect.width / 2, event.rect.height / 2, 100 * (event.rect.width / 100), 75 * (event.rect.height / 75));

		target.style.webkitTransform = target.style.transform =
			'translate(' + x + 'px,' + y + 'px)';

		target.setAttribute('data-x', x);
		target.setAttribute('data-y', y);
	}
}
*/

function canvasPixelPositionToPercent(x, y) {
	var canvasWidth = (parseFloat(getCanvas().style.width) || 0);
	var canvasHeight = (parseFloat(getCanvas().style.height) || 0);

	var positionInPercent = {
		x: 0,
		y: 0,
	}

	positionInPercent.x = (x / canvasWidth) * 100;
	positionInPercent.y = (y / canvasHeight) * 100;
	return positionInPercent;
}

class Vectorgraphic extends Shape {
	constructor(elementToClone, htmlId) {
		super(elementToClone, htmlId);
		//SVGInject(this.element);
		this.element.classList.add('vectorgraphic');
		this.element.setAttribute('preserveaspectratio', 'none');
		this.element.style.width = 20 * (9 / 16) * (elementToClone.naturalWidth / elementToClone.naturalHeight) + '%';
		this.element.style.height = '20%';
		// this.element.setAttribute('width', 20 * (9 / 16) * (elementToClone.naturalWidth / elementToClone.naturalHeight) + '%');
		// this.element.setAttribute('height', '20%');
		this.element.style['transform-origin'] = 'center center';
		this.element.style['transform'] = 'translate(-50%,-50%)';
	}

	redraw(event) {
		console.log('Redraw the vector graphic');

		let shiftKey = event.shiftKey;

		var target = this.element,
			x = (parseFloat(target.getAttribute('data-x')) || 0),
			y = (parseFloat(target.getAttribute('data-y')) || 0);


		var resizeRect = canvasPixelPositionToPercent(event.rect.width, event.rect.height);
		// update the element's style
		var width;
		var height;
		if (shiftKey) {
			let aspectRatio = (9 / 16) * (this.element.naturalWidth / this.element.naturalHeight);
			if (resizeRect.x > resizeRect.y) {
				width = resizeRect.x * aspectRatio + '%';
				height = resizeRect.x + '%';
			}
			else {
				width = resizeRect.y * aspectRatio + '%';
				height = resizeRect.y + '%';
			}
		}
		else {
			width = resizeRect.x + '%';
			height = resizeRect.y + '%';
		}

		event.target.style.width = width;
		event.target.style.height = height;
		target.style.width = width;
		target.style.height = height;

		updateValueInTab('width', parseFloat(width).toFixed(2));
		updateValueInTab('height', parseFloat(height).toFixed(2));
	}
}

class TextField extends Shape {
	constructor(elementToClone, htmlId) {
		super(elementToClone, htmlId);
		// this.element.classList.add('vectorgraphic');
		// this.element.setAttribute('preserveaspectratio', 'none');
		this.element.style.width = '20%';
		this.element.style.height = '20%';
		
		let fontSize = elementToClone.style.fontSize;
		if(fontSize.endsWith('%'))
		{
			this.element.style.fontSize = fontSize;
		}
		else this.element.style.fontSize = '10%';

		this.element.style['transform-origin'] = 'center center';
		this.element.style['transform'] = 'translate(-50%,-50%)';
		this.element.style['line-height'] = '100%';
		this.element.classList.add('textField');

		if(!elementToClone.classList.contains('textField')) {
			console.log('Does not declare class textField')
			this.element.classList.add('textField');
		} else {
			console.log('Already declares class textField');
		}
		
	}

	redraw(event) {
		console.log('Redraw the textfield');

		let shiftKey = event.shiftKey;

		var target = this.element,
			x = (parseFloat(target.getAttribute('data-x')) || 0),
			y = (parseFloat(target.getAttribute('data-y')) || 0);


		var resizeRect = canvasPixelPositionToPercent(event.rect.width, event.rect.height);
		// update the element's style
		var width;
		var height;
		if (shiftKey) {
			let aspectRatio = 1;
			if (resizeRect.x > resizeRect.y) {
				width = resizeRect.x * aspectRatio + '%';
				height = resizeRect.x + '%';
			}
			else {
				width = resizeRect.y * aspectRatio + '%';
				height = resizeRect.y + '%';
			}
		}
		else {
			width = resizeRect.x + '%';
			height = resizeRect.y + '%';
		}

		event.target.style.width = width;
		event.target.style.height = height;
		target.style.width = width;
		target.style.height = height;

		updateValueInTab('width', parseFloat(width).toFixed(2));
		updateValueInTab('height', parseFloat(height).toFixed(2));
	}
}