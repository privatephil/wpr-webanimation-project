function copyToClipboard(string) {
	let inputField = document.createElement('input');
	inputField.value = string;
	document.body.appendChild(inputField);
	inputField.select();
	document.execCommand('copy');
	document.body.removeChild(inputField);
	var toastHTML = '<span style="color: rgb(131, 198, 134);">Copied to Clipboard</span>';
	M.toast({html: toastHTML, displayLength: 1200});

}

(function ($) {


   $(function () {

      $('.sidenav').sidenav();
      $('.modal').modal();

      $('.collapsible').collapsible();
      $('.dropdown').dropdown();
      $('select').formSelect();

      $('.collapsible').collapsible('open', 0);

      $('.tooltipped').tooltip({ enterDelay: 450 });

      $('#sortselect').on('change', function () {
         this.form.submit();
      });

      /* wurde durch neuen reinen html button ersetzt
      $('#sortSelectAnimations').on('change', function () {
         this.form.submit();
      });
      */

      if ($.trim('#input-outer input').value != "")
         $('#input-outer input').next('div.show_hide').show();
      else
         $('#input-outer input').next('div.show_hide').hide();





      $('#input-outer input').on('input', function () {
         if ($.trim(this.value) != "")
            $(this).next('div.show_hide').show();
         else
            $(this).next('div.show_hide').hide();
      });



      $('#clear').click(function () {
         $('#input-outer input').val('');
         $('#input-outer input').focus();
         $('.show_hide').hide();
         $('#input-outer input').removeAttr('placeholder');
         //Redirect muss noch angepasst werden
         window.location = 'http://localhost:8888/lessons';
      });


      $('#copyShareLinkToClipboard').bind('click', function() {
         let shareLink = $('#shareLink').val();
         copyToClipboard(shareLink);
         alert("The following share link was copied to your clipboard\r\n\r\n" + shareLink);
      });

      $('.shareLinkCopyMaker').bind('click', function(event) {
         event.stopPropagation();
         let shareLink = $(this).find('input').val();
         copyToClipboard(shareLink);
         alert("The following share link was copied to your clipboard\r\n\r\n" + shareLink);
      });

      console.log('ende onload init.js');
   }); // end of document ready
})(jQuery); // end of jQuery name space



