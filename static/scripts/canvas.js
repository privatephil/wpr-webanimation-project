let shapesList = new ShapeList();
let deletedShapes = [];
let keyframesManager = new KeyframesManager(shapesList);

let player = null;

let kfStartDataX = 0.0;
let editmode = false;

let thumbnail;


/* _________  HELPERS  ___________ */

function getCanvas() {
	return document.getElementById('canvas');
}

// Converts from degrees to radians.
Math.radians = function (degrees) {
	return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = function (radians) {
	return radians * 180 / Math.PI;
};

function simulateShiftClick(shiftKey, elem) {
	// // Create our event (with options)
	var evt = new MouseEvent('click', {
		bubbles: true,
		cancelable: false,
		view: window,
		shiftKey: shiftKey,
	});
	// If cancelled, don't dispatch our event
	if(elem && event){
		var cancled = elem.dispatchEvent(evt);
		// console.log(cancled);
	}
};

function simulateDblClick(elem) {
	// // Create our event (with options)

	
	var evt = new MouseEvent('dblclick', {
		bubbles: true,
		cancelable: false,
		view: window,
	});
	// If cancelled, don't dispatch our event
	if(elem && evt){
		var cancled = elem.dispatchEvent(evt);
		// console.log(cancled);
	}
};

/* Transform Tool Init */
function transformToolSelect(){

	var tt = document.getElementById('transformTool');
	var selectedShapes = shapesList.getSelected()
	var bounds = shapesList.getSelectedBounds();

	if(selectedShapes.length == 0){
		transformToolDeselect();
		return;
	}

	// Calc Relative Measures
	var size = canvasPixelPositionToPercent(bounds.width, bounds.height);
	var position = canvasPixelPositionToPercent(bounds.left, bounds.top);

	// Show TransformationTool
	tt.style.display = 'inline-block';
	// Set TransformTool Size and Position
	// Take Bounds Size if >1 shape selected
	if(selectedShapes.length > 1){
		tt.style.width = size.x + '%';
		tt.style.height = size.y + '%';
		tt.style.transform = 'translate(-50%, -50%) rotate( 0rad )';
	}
	// Take Element Size if 1 shape selected
	else{
		tt.style.width = selectedShapes[0].element.style.width;
		tt.style.height = selectedShapes[0].element.style.height;

		// var example = 'translate(-50%, -50%) rotate(90.2rad)';
		var transform = selectedShapes[0].element.style.transform;
		let regex = new RegExp('rotate.([+-]?([0-9]*[.])?[0-9]+)rad');
		let res = regex.exec(transform);
		
		// OLD Way
		// var angle = selectedShapes[0].element.getAttribute('data-angle');
		if(res){
			let angle = parseFloat(res[1]);
			console.log('Rotation Angle:' + parseFloat(res[1]));
			tt.style.transform = `translate(-50%, -50%) rotate( ${angle}rad )`;
			tt.setAttribute('data-angle', angle);
		}
		else {
			tt.style.transform = 'translate(-50%, -50%) rotate( 0rad )';
			tt.setAttribute('data-angle', 0);
		}
	}
	
	// Important for correct positioning when moved by anime.js
	tt.setAttribute('data-x', position.x);
	tt.setAttribute('data-y', position.y);
	selectedShapes[0].element.setAttribute('data-x', selectedShapes[0].element.style.left);
	selectedShapes[0].element.setAttribute('data-y', selectedShapes[0].element.style.top);
	
	// Set position
	tt.style.left = position.x + '%';
	tt.style.top = position.y + '%';

	tt.setAttribute('data-target', selectedShapes[0].element.id);
}
/* Transform Tool Deselect */
function transformToolDeselect(){
	var tt = document.getElementById('transformTool');

	// Display none
	tt.style.display = 'none';

	tt.setAttribute('data-target', '');
}


var transformToolActionHappened = false;
var snap = false;
/* Interact Initialization*/
function initInteractJs() {
	/* Direct Shape Drag Initializaon */
	interact('.shape')		
	.draggable({
	  onmove: shapeDragMoveListener,
	  modifiers: [
	    interact.modifiers.restrict({
	      restriction: 'parent',
	      elementRect: {
	        top: 0,
	        left: 0,
	        bottom: 1,
	        right: 1
	      }
	    })
	  ],
	})
	// // 	.on('click', selectShape);


	/* Transform Tool Initialization*/
	interact('.resizable')
		.draggable({
			onmove: ttDragMoveListener,
			modifiers: [
				interact.modifiers.restrict({
					restriction: 'parent',
					elementRect: {
						top: 0,
						left: 0,
						bottom: 1,
						right: 1
					}
				})
			],
			inertia: true
		})
		.resizable({
			// resize from all edges and corners
			edges: { left: '.resize-left', right: '.resize-right', bottom: '.resize-bottom', top: '.resize-top' },

			modifiers:
				[
					// keep the edges inside the parent
					interact.modifiers.restrictEdges({
						outer: 'parent',
						endOnly: false,
					}),

					// minimum size
					interact.modifiers.restrictSize({
						min: { width: 20, height: 20 },
					})
				],
			inertia: true

			})
			.on('resizemove', function (event) {
				transformToolActionHappened = true;
				var selectedShapes = shapesList.getSelected();
				shapesList.getShape(selectedShapes[0].element.id).redraw(event);
			})
			.on('click', function (event){
				// Click through Transformation Tool if drag has not happened
				// console.log('Click:' + transformToolActionHappened);
				if(!transformToolActionHappened){
					event.target.style.pointerEvents = 'none';
					var clickThrough = document.elementFromPoint(event.clientX,event.clientY);
					simulateShiftClick(event.shiftKey, clickThrough);
					event.target.style.pointerEvents = 'auto';
				}
					// Reset Happening
					transformToolActionHappened = false;
			})			
			.on('dblclick', function (event){
				event.target.style.pointerEvents = 'none';
				var clickThrough = document.elementFromPoint(event.clientX,event.clientY);
				simulateDblClick(clickThrough);
				event.target.style.pointerEvents = 'auto';
				// console.log('Hello');
				

			})
			.on('dragmove', function (event){
				transformToolActionHappened = true;
				// console.log('Drag:' + transformToolActionHappened);
				
			})
			;

	/* Rotation Handle Initialization*/
	interact('.rotation-handle')
		.draggable({
			onstart: function (event) {

				var box = event.target.parentElement;
				var rect = box.getBoundingClientRect();
				var target = document.getElementById(event.target.parentElement.getAttribute('data-target'));

				// store the center as the element has css `transform-origin: center center`
				box.setAttribute('data-center-x', rect.left + rect.width / 2);
				box.setAttribute('data-center-y', rect.top + rect.height / 2);
				target.setAttribute('data-center-x', rect.left + rect.width / 2);
				target.setAttribute('data-center-y', rect.top + rect.height / 2);
				// get the angle of the element when the drag starts
				var angle = getDragAngle(event);
				box.setAttribute('data-angle', angle);
				target.setAttribute('data-angle', angle);

			},
			onmove: function (event) {
				var box = event.target.parentElement;
				var target = document.getElementById(event.target.parentElement.getAttribute('data-target'));

				var pos = {
					x: parseFloat(box.getAttribute('data-x')) || 0,
					y: parseFloat(box.getAttribute('data-y')) || 0
				};

				var angle = getDragAngle(event);


				// update transform style on dragmove
				box.style.transform = 'translate(-50%, -50%) rotate(' + angle + 'rad' + ')';
				target.style.transform = 'translate(-50%, -50%) rotate(' + angle + 'rad' + ')';
				updateValueInTab('rot', Math.degrees(angle).toFixed(2));
				transformToolActionHappened = true;
			},
			onend: function (event) {
				var box = event.target.parentElement;
				var target = document.getElementById(event.target.parentElement.getAttribute('data-target'));
				var angle = getDragAngle(event);

				// save the angle on dragend
				target.setAttribute('data-angle', angle);
				box.setAttribute('data-angle', angle);
			},
		})

	function getDragAngle(event) {
		var box = event.target.parentElement;
		var startAngle = parseFloat(box.getAttribute('data-angle')) || 0;
		var center = {
			x: parseFloat(box.getAttribute('data-center-x')) || 0,
			y: parseFloat(box.getAttribute('data-center-y')) || 0
		};
		var angle = Math.atan2(center.y - event.clientY, center.x - event.clientX);
		angle = angle - startAngle;

		// lock angle if shift pressed
		let shiftKey = event.shiftKey;
		let lockAngle = 15;
		if (shiftKey) {
			var angleDeg = Math.degrees(angle);
			var calcAngle = lockAngle * Math.round(angleDeg / lockAngle);
			angle = Math.radians(calcAngle);
		}

		return angle;
	}


	/* Keyframe Drag */

	interact('.keyframe')
	.draggable({
	onmove: dragKeyframeListener,
	modifiers: [
		interact.modifiers.restrict({
		restriction: 'parent',
		elementRect: {
			top: 0,
			left: 0,
			bottom: 1,
			right: 1
		}
		})
	]
	}).on('dragstart', keyframeDragStart).on('dragend', keyframeDragEnd);

}

function initSVGInject() {
	SVGInject.setOptions({ makeIdsUnique: false });
	SVGInject(document.getElementsByClassName('shapecreator'));
}

function registerEventHandlers() {

	$('#download').bind('click', download);
	$('#setThumbnail').bind('click', setThumbnail);

	$('#undoDeletion').bind('click', undoDeletion);
	$('#createKeyframe').bind('click', createKeyframe);
	$('#deleteKeyframe').bind('click', deleteKeyframe);
	$('#freezeKeyframes').bind('click', freezeKeyframes);
	$('#keyframeArea').on('click', '.keyframe', selectKeyframe)
	$('#keyframeArea').bind('click', deselectKeyframes);

	$('#shapes-category-container').on('click', '.shapecreator', createShape);
	$('#icons-category-container').on('click', '.shapecreator', createShape);
	$('.textFieldCreator').on('click', createTextField);

	$('#canvas').on('click', '.shape', selectShape);
	$('#canvas').bind('click', canvasDeselect);

	$('#canvas').on('dblclick', '.textField', textFieldEnter);
	$('#canvas').on('blur', '.textField', textFieldLeave);

	
	$('#animation-title').on('input', dynamicTitleLength);
	$('#animation-title').on('focus', () => {editmode = true});
	$('#animation-title').on('focusout', () => {editmode = false});

	$('.play-arrow').bind('click', playButtonClick);
	$('.grid').bind('click', gridButtonClick);
	$('#showFullNav').bind('click', showFullNavigation);
	$('#hideFullNav').bind('click', hideFullNavigation);

	document.addEventListener('keydown', shortcuts);

	$('#saveAnimation').bind('click', saveAnimation);
}

function showFullNavigation(){
	let nav = document.getElementById('fullscreenNav');
	nav.style.display= 'block';
	nav.style.pointerEvents= 'auto';
	nav.style.opacity = 0.5;
	nav.style.bottom = '10%';
	nav.style.transform = 'scale(1)'
	
}

function hideFullNavigation(){
	let nav = document.getElementById('fullscreenNav');
	nav.style.opacity = 0;
	nav.style.bottom = '-50px';
	nav.style.transform = 'scale(0.01)'
	setTimeout( () => {
		let nav = document.getElementById('fullscreenNav');
		nav.style.display= 'none';
		nav.style.pointerEvents= 'none';
	}, 500)
}

function canvasDeselect(event){
	if(!transformToolActionHappened){
		deselectShapes(event);
	}
}

function textFieldEnter(event) {
	if(!editmode) {
		var element = $(`#${event.currentTarget.id}`);
		var textarea = document.createElement('textarea');
		textarea.style.fontFamily = $(element)[0].style.fontFamily;
		textarea.style.fontSize = '100%';
		$(textarea).html($(element).html().replace(/<br>/g, '\r\n'));
		$(element).html('');
		$(element).append(textarea);
	
		$(event.currentTarget).find('textarea').focus();
		$(event.currentTarget).find('textarea').select();
		editmode = true;
	}
}

function textFieldLeave(event){
	console.log('You were here?');
	$(event.currentTarget).html(escapeHtml($(event.currentTarget).find('textarea').val()).replace(/\r\n|\r|\n/g, '<br>'));
	editmode = false;
}

function escapeHtml(string) {
	return string
	.replace(/&/g, '&amp;')
	.replace(/>/g, '&gt;')
	.replace(/</g, '&lt;')
	.replace(/"/g, '&quot;')
	.replace(/'/g, '&apos;');
}


function dynamicTitleLength(event){

	if(event){
		event.target.style.width = (event.target.value.length+1) * 0.8 + 'ch';
	}
	else{
		var title = document.getElementById('animation-title');
		title.style.width = (title.value.length+1) * 0.8 + 'ch';
	}
}

window.onload = function () {
	initInteractJs();
	registerEventHandlers();
	//initSVGInject();

	console.log('player init');
	player = new AnimationPlayer(shapesList);

	// Set Title Width
	dynamicTitleLength(null);
	setTimeGrid(wholeDuration);

	loadAnimationData(document.getElementById('potionDataHiddenInputText').value);
}

/* Functionality */
function createShape(event) {
	if (event.currentTarget.classList.contains('shapecreator')) {
		if(event.currentTarget.getAttribute('src').endsWith('.svg')) {
			var shape = new Vectorgraphic(event.currentTarget, null);
		}
		else {
			var shape = new Shape(event.currentTarget, null);
		}
		
		shapesList.addShape(shape);
		shapesList.deselectAll();
		transformToolSelect();
	}

	updatePropertiesTab(1);
}

function createTextField(event) {
	if (event.currentTarget.classList.contains('textFieldCreator')) {
		let shape = new TextField(event.currentTarget, null);
		shapesList.addShape(shape);
		shapesList.deselectAll();
		transformToolSelect();
	}

	updatePropertiesTab(1);
}

function canvasPixelPositionToPercent(x, y) {
	var canvasWidth = (parseFloat(getCanvas().style.width) || 0);
	var canvasHeight = (parseFloat(getCanvas().style.height) || 0);

	var positionInPercent = {
		x: 0,
		y: 0,
	}

	positionInPercent.x = (x / canvasWidth) * 100;
	positionInPercent.y = (y / canvasHeight) * 100;
	return positionInPercent;
}

/*
	Listener functions
*/
function shapeDragMoveListener(event) {

	event.preventDefault();

	var target = event.target;
	// keep the dragged position in the data-x/data-y attributes

	var canvasPositionInPercent = canvasPixelPositionToPercent(event.dx, event.dy);
	var x = (parseFloat(target.style.left) || 0) + canvasPositionInPercent.x;
	var y = (parseFloat(target.style.top) || 0) + canvasPositionInPercent.y;

	// translate the elements
	//target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
	target.style.left = x + '%';
	target.style.top = y + '%';

	// update the posiion attributes
	target.setAttribute('data-x', x);
	target.setAttribute('data-y', y);
}

function ttDragMoveListener(event) {
	event.preventDefault();
	event.stopPropagation();

	// keep the dragged position in the data-x/data-y attributes
	var deltaPercent = canvasPixelPositionToPercent(event.dx, event.dy);
	var target;
	var selectedShapes = shapesList.getSelected();
	
	// Position every selected Shape
	for (i = 0; i < selectedShapes.length; i++) {
		target = selectedShapes[i].element;
		var x = parseFloat(target.getAttribute('data-x') || 0) + deltaPercent.x;
		var y = parseFloat(target.getAttribute('data-y') || 0) + deltaPercent.y;
		
		// Snap to Grid
		var snapGrid = 5; 
		if(snap){
			var nx = snapGrid * Math.round(x / snapGrid);
			var ny = snapGrid * Math.round(y / snapGrid);
			// translate the elements
			target.style.left = nx + '%';
			target.style.top = ny + '%';
		}
		else{
		// translate the elements
		target.style.left = x + '%';
		target.style.top = y + '%';
		}
		// update the posiion attributes
		target.setAttribute('data-x', x);
		target.setAttribute('data-y', y);

	}
	
	// Get TransformTool Position
	var bounds = shapesList.getSelectedBounds();
	var position = canvasPixelPositionToPercent(bounds.left, bounds.top);
	event.target.style.left = position.x + '%';
	event.target.style.top = position.y + '%';
	event.target.setAttribute('data-x', position.x);
	event.target.setAttribute('data-y', position.y);

	//update the Values in Properties Tab
	updateValueInTab('left', position.x.toFixed(2));
	updateValueInTab('top', position.y.toFixed(2));
	
}

function dragKeyframeListener(event) {
	event.preventDefault();
	let keyframeArea = document.getElementById('keyframeArea');
	let areaWidth = keyframeArea.getBoundingClientRect().width

	var dx = event.dx / areaWidth * 100;

	var target = event.target,
	// keep the dragged position in the data-x attribute
	x = (parseFloat(target.getAttribute('data-x')) || 0) + dx;
	
	target.style.left = x + '%';

	// update the position attributes
	target.setAttribute('data-x', x);
	//set the timeline pointer to the keyframeposition
	player.gotoTime(event.currentTarget.getAttribute('data-x')); 

}

function keyframeDragStart(event) {
	selectKeyframe(event);

	let element = event.target;
	kfStartDataX = parseFloat(element.getAttribute('data-x'));
}

function keyframeDragEnd(event) {
	let selectedShapes = shapesList.getSelected();

	for (let i = 0; i < selectedShapes.length; i++) {
		let kf = selectedShapes[i].getKeyframe(event.target.id);

		if (kf) {

			let element = kf.getLiveHTMLElement();
			let dataX = parseFloat(element.getAttribute('data-x'));

			kf.changeValueOfTimeLinePointer(kfStartDataX, dataX);

		}
	}
	
	// Set New relative Time
	let draggedKeyframe = keyframesManager.getKeyframe(event.target.id);
	let x = event.target.getAttribute('data-x');
	if(x){
		draggedKeyframe.setRelativeTimeValue(x);
	}

	// Sort Keyframes for all Shapes
	let allShapes = shapesList.getAll();
	if(allShapes){
		for(let i = 0; i < allShapes.length ; i++){
			allShapes[i].sortKeyframes();
		}
	}

	player.setAnimations();	
}

function selectShapesById(event, id){
	let shiftKey = event.shiftKey;
	let ptButton = document.getElementById('propertiesTabButton');

		if(shiftKey) {
			let shape = shapesList.getShape(id);

			if(!shape.isSelected()) {			// not selected yet -> select it
				console.log('Select');
				shape.select();
				transformToolSelect();
				setKeyframeButton(true);
				setFreezeButton(true);
				updatePropertiesTab(2, shape.element);
				ptButton.click();

			} else { 							// already selected - deselect it
				console.log('Deselect');
				shape.deselect();
				transformToolSelect();
				setKeyframeButton(false);
				setFreezeButton(false);
				transformToolDeselect();
				updatePropertiesTab(3);
			}		
		} else {								// select just one shape
			shapesList.deselectAll();
			let shape = shapesList.getShape(id);
			shape.select();
			transformToolSelect();
			setKeyframeButton(true);
			setFreezeButton(true);
			ptButton.click();
			showProperties(shape.element);
		}
	
	
}

function selectShape(event) {
	let shiftKey = event.shiftKey;
	let propertiesButton = document.getElementById('propertiesTabButton');
	if(event.currentTarget.id !== 'canvas') {
		
		let shape = shapesList.getShape(event.currentTarget.getAttribute('id'));

		// Shift pressed
		if(shiftKey) {
			
			if(!shape.isSelected()) {			// not selected yet -> select it
				console.log('Select');
				shape.select();
				transformToolSelect();
				setKeyframeButton(true);
				setFreezeButton(true);

				updatePropertiesTab(2, event.currentTarget);
				propertiesButton.click();

			} else { 							// already selected - deselect it
				console.log('Deselect');
				shape.deselect();
				transformToolSelect();
				setKeyframeButton(false);
				setFreezeButton(false);
				updatePropertiesTab(3);
			}

		} 
		// Shift NOT Pressed
		else {									// select just one shape
			shapesList.deselectAll();
			shape.select();
			transformToolSelect();
			setKeyframeButton(true);
			setFreezeButton(true);
			showProperties(event.currentTarget);
			propertiesButton.click();
		}
	}
	
}

function selectAllShapes(event) {
	shapesList.selectAll();
	setKeyframeButton(true);
	setFreezeButton(true);
	transformToolSelect(event);
}

function deselectShapes(event) {

		if (event.target.id === 'canvas' || event.key === 'Escape') {
			shapesList.deselectAll();
			resetElementsList();
			setKeyframeButton(false);
			setFreezeButton(false);
			setKeyframeDeleteButton(false);
			transformToolDeselect();
		}

}

function deselectAllShapes() {
	shapesList.deselectAll();
	resetElementsList();
	setKeyframeButton(false);
	setFreezeButton(false);
	setKeyframeDeleteButton(false);
	transformToolDeselect();
}

function selectKeyframe(event) {

	RemoveUpdateKeyframeButton();

	if (event.currentTarget.id !== 'keyframeArea') {
		RemoveUpdateKeyframeButton();
		let shiftKey = event.shiftKey;
		let keyframe = keyframesManager.getKeyframe(event.currentTarget.getAttribute('id'));
		let selectedKeyframes;

		// Shift pressed
		/*if(shiftKey) {
			
			if(!keyframe.isSelected()) {			// not selected yet -> select it
				keyframe.select();
				setKeyframeDeleteButton(true);

			} else { 							// already selected - deselect it
				keyframe.deselect();

				// if no keyframe selected -> deavtivate delete
				selectedKeyframes = keyframesManager.getSelected();
				if(selectedKeyframes.length == 0){
					setKeyframeDeleteButton(false);
				}
			}

		} */
		// Shift NOT Pressed
		//else {									// select just one shape
			keyframesManager.deselectAll();
			keyframe.select();
			player.gotoTime(event.currentTarget.getAttribute('data-x')); 
			updateAllValues();
			setKeyframeDeleteButton(true);
			AddUpdateKeyframeButton();
		//}
	}
}

function deselectKeyframes(event) {

	//TODO: now deselect keyframes through currently selected shapes
	if (event.target.id === 'keyframeArea') {
		keyframesManager.deselectAll();
		RemoveUpdateKeyframeButton();

		setKeyframeDeleteButton(false);
	}
}

//update the values of the selected keyframes!
function updateSelectedKeyframe(event) {
	let keyframe = keyframesManager.getSelected()[0];
	let selectedShape = shapesList.getSelected()[0];
	let selectedElement = selectedShape.element;
	
	keyframe.updateValues(selectedElement);
	keyframesManager.deselectAll();
	RemoveUpdateKeyframeButton();
	selectedShape.sortKeyframes();
	player.setAnimations();
}

/* Keyframe Functionality */
function setKeyframeButton(yesno) {
	// handle keyframe buttons for selected object
	createKeyframeButton = document.getElementById('createKeyframe');
	if (yesno) {
		if (createKeyframeButton.classList.contains('disabled')) {
			createKeyframeButton.classList.remove('disabled');
		}
	} else {
		if (!createKeyframeButton.classList.contains('disabled')) {
			createKeyframeButton.classList.add('disabled');
		}
	}
}

function setKeyframeDeleteButton(yesno) {
	// handle keyframe buttons for selected object
	deleteKeyframeButton = document.getElementById('deleteKeyframe');

	if (yesno) {
		deleteKeyframeButton.classList.remove('disabled');
	} else {
		deleteKeyframeButton.classList.add('disabled');
	}
}

function setFreezeButton(yesno) {
	// handle keyframe buttons for selected object
	// console.log("SET FREEZE BUTTON!");
	createKeyframeButton = document.getElementById('freezeKeyframes');

	if (yesno) {
		if (createKeyframeButton.classList.contains('disabled')) {
			createKeyframeButton.classList.remove('disabled');
		}
	} else {
		if (!createKeyframeButton.classList.contains('disabled')) {
			createKeyframeButton.classList.add('disabled');
		}
	}
}

function createKeyframe() {
	let valueOfTimelinePointer = document.getElementById('timelinePointer').value;
	let selectedShapes = shapesList.getSelected();

	let position = {};

	let rotation; 	// float representing the degree the shape is rotated
	let scale; 	// float representing the scale of the shape

	let isFirst;

	for (let i = 0; i < selectedShapes.length; i++) {
		// position.x = parseFloat(selectedShapes[i].element.getAttribute('data-x'));
		// position.y = parseFloat(selectedShapes[i].element.getAttribute('data-y'));
		//let zIndex =  parseFloat(selectedShapes[i].element.style.zIndex);

				//get Element's Rotation
				let regex = new RegExp('rotate.([+-]?([0-9]*[.])?[0-9]+)rad');
				let res = regex.exec(selectedShapes[i].element.style.transform);
				
		if(res) {var thisRotation = parseFloat(res[1]);}
		else {var thisRotation = 0;}
		

		let values = {
			position: {
				x: parseFloat(selectedShapes[i].element.style.left),
				y: parseFloat(selectedShapes[i].element.style.top)
			},

			scale: {
				x: parseFloat(selectedShapes[i].element.style.width) + '%',
				y: parseFloat(selectedShapes[i].element.style.height) + '%'
			},

			rotation: parseFloat(thisRotation),

			opacity: parseFloat(selectedShapes[i].element.style.opacity)
		};

		//isFirst = selectedShapes[i].getKeyframes().length === 0;
		isFirst = false;
		isLast = false;
		console.log(isFirst);
		console.log('position: ' + values.position.x + ', ' + values.position.y)

		//TODO: for shape in selectedShapes do createKeyframe
		selectedShapes.forEach(shape => {
			shape.createKeyframe(valueOfTimelinePointer, values, isFirst, isLast);
		});
	}
	keyframesManager.deselectAll();
	player.setAnimations();
}

function deleteKeyframe(event) {

	//TODO: now delete the keyframe through currently selected shapes

	keyframesManager.deleteKeyframes();
	keyframesManager.deselectAll();
	setKeyframeDeleteButton(false);
	player.setAnimations();
	transformToolSelect();
	RemoveUpdateKeyframeButton();

}

/* Shortcuts */
function shortcuts(event) {
	let ctrlDown = event.ctrlKey
	let cmdDown = event.metaKey;
	let keyDown = event.key;

	// SPACEBAR -> Play Animation
	let prohibitedTargets = ['TEXTAREA', 'INPUT']
	
	if(event.keyCode == 32 && (!prohibitedTargets.includes(event.target.nodeName) || event.target.type == 'range')){
		
		let playButton = document.getElementById('playButton');
		if(playButton.value == 0 || playButton.value == undefined){
			playButtonClick(event);
		}
		else{
			playButtonClick(event);
		}
	}

	// CRTL + A  -> Select All Shapes
	if ( (ctrlDown || cmdDown) && keyDown === 'a' && !prohibitedTargets.includes(event.target.nodeName)) {
		event.preventDefault();
		deselectShapes(event);

		selectAllShapes();
		let propertiesButton = document.getElementById('propertiesTabButton');
		updatePropertiesTab(3, event.currentTarget);
		propertiesButton.click();
		transformToolSelect();

	// ESC  ->  Deselect All Shapes 
	} else if (keyDown === 'Escape') {
		event.preventDefault();
		deselectShapes(event);
		resetElementsList();
		transformToolDeselect();
		if(editmode){
			$('*').blur();
		}

	// CRTL + BACKSPACE  ->  DELETE Selected Shapes
	} else if ( (ctrlDown || cmdDown) && keyDown === 'Backspace') {
		console.log('backspace gedrück');
		event.preventDefault();

		deleteSelectedShapes();
	} else if (event.shiftKey )
	{
		if(event.keyCode == 49) //Shift + 1
		{
			jumpToStart();
		}
		else if (event.keyCode == 50) // Shift + 2
		{
			jumpToEnd();
		}
		else if (event.keyCode == 37) //Shift + LEFT
		{
			jumpBackward();
		}
		else if (event.keyCode == 39) //Shift + RIGHT
		{
			jumpForward();
		}
	}
}

function deleteSelectedShapes()
{
	let selected = shapesList.getSelected();

			// copy shapes into the list of deleted shapes
			selected.forEach(sh => {
				let target = sh.element;
				let wrap = document.createElement('div');
				wrap.appendChild(target.cloneNode(true));
	
				/* Needs to be done, because chrome (and maybe other browsers) create quotes around font declarations */
				sh.htmlText = wrap.innerHTML.replace(/&quot;/g, '');
	
				deletedShapes.push(sh);
			});

					// console.log(selected);
		shapesList.removeShapes(selected);
		resetElementsList(); //not update, because all selected Items will be deleted!
		transformToolDeselect();
		setKeyframeButton(false);
		setFreezeButton(false);
		setKeyframeDeleteButton(false);

}

function undoDeletion() {
	let obj = deletedShapes.pop();
	console.log('item: ' + JSON.stringify(obj));

	shapesList.loadObj(obj);
	deselectAllShapes();
	shapesList.updateShapeIds();
	player.setAnimations();
}

/*
	GUI related
*/

function playButtonClick(event) {
	//console.log('playButtonClick');
	var livePlayArrows = document.getElementsByClassName('play-arrow');

	if(livePlayArrows[0].value == 0 ||  livePlayArrows[0].value == null){
		for (var i = 0; i < livePlayArrows.length; i++) {
			livePlayArrows[i].innerHTML = 'pause';
			livePlayArrows[i].value = 1;
		}
		resetElementsList();
		startAnimation(event);
	}
	else if(livePlayArrows[0].value == 1){
		for (var i = 0; i < livePlayArrows.length; i++) {
			livePlayArrows[i].innerHTML = 'play_arrow';
			livePlayArrows[i].value = 0;
		}
		resetElementsList();
		pauseAnimation(event);
	}
}

function gridButtonClick(event) {
	var liveGridOff = document.getElementsByClassName('grid');

	if(liveGridOff[0].value == 0 ||  liveGridOff[0].value == null){
		for (var i = 0; i < liveGridOff.length; i++) {
			liveGridOff[i].innerHTML = 'grid_on';
			liveGridOff[i].value = 1;
		}
		resetElementsList();
		snap = true;
		document.getElementById('grid').style.opacity = 0.25;
	}
	else if(liveGridOff[0].value == 1){
		for (var i = 0; i < liveGridOff.length; i++) {
			liveGridOff[i].innerHTML = 'grid_off';
			liveGridOff[i].value = 0;
		}
		resetElementsList();
		snap = false;
		document.getElementById('grid').style.opacity = 0;
	}
}

function startAnimation(event) {
//console.log('startAnimation')

  shapesList.deselectAll();
  transformToolDeselect();
//   deselectShapes();
  player.setAnimations();
  player.play();
}

function pauseAnimation(event) {
	//console.log('pauseAnimation')
	player.pause();
}

function jumpToStart(){
	var timeline = document.getElementById("timelinePointer");
	timeline.value = 0;
	player.gotoTime(timeline.value);
  }
  
function jumpToEnd(){
	var timeline = document.getElementById("timelinePointer");
	timeline.value = 100;
	player.gotoTime(timeline.value);
}
  
  function jumpForward(){ //Macht noch Probleme
	  var timeline = document.getElementById("timelinePointer");
	  var val = parseFloat(timeline.value) + 0.1;
	  timeline.value = val;
	  player.gotoTime(val);
  }
  
  function jumpBackward(){
	  var timeline = document.getElementById("timelinePointer");
	  var val = parseFloat(timeline.value) - 0.1;
	  timeline.value = val;
	  player.gotoTime(timeline.value);
  }

// replacer function for JSON stringify
function replacer(key,value) {
	if (key=="animation") return undefined;
    else return value;
}

function download(event) {
	console.log('copy scene to clipboard');
	let obj = JSON.stringify(shapesList, replacer);
	copyToClipboard(obj);
}

/*
 * Copies the given string directly to clipboard.
 */
function copyToClipboard(string) {
	let inputField = document.createElement('input');
	inputField.value = string;
	document.body.appendChild(inputField);
	inputField.select();
	document.execCommand('copy');
	document.body.removeChild(inputField);
	var toastHTML = '<span style="color: rgb(131, 198, 134);">Copied to Clipboard</span>';
	M.toast({html: toastHTML, displayLength: 1200});

}

/*
 * Sets the current frame of canvas as the thumbnail of the animation
 */
function setThumbnail() {
	console.log("set Thumbnail");

	let element = getCanvas();
	let g = document.getElementById('grid')
	let tt = document.getElementById('transformTool')
	let nav = document.getElementById('fullscreenNav')
	let navB = document.getElementById('showNavButton')

	// remove grid and transformtool from canvas
	document.getElementById('grid').remove();
	document.getElementById('transformTool').remove();
	document.getElementById('fullscreenNav').remove();
	document.getElementById('showNavButton').remove();

	html2canvas(element).then(function(canvas) {
	    // Export the canvas to its data URI representation
	    thumbnail = canvas.toDataURL("image/png");	
	});

	// add grid and transformtool back to canvas
	element.appendChild(g);
	element.appendChild(tt);
	element.appendChild(nav);
	element.appendChild(navB);
	
	
	let toastHTML = '<span style="color: rgb(131, 198, 134);">New Thumbnail set</span>';
	M.toast({html: toastHTML, displayLength: 1200});
}

function saveAnimation() {

	let xhttp = new XMLHttpRequest();
	console.log(window.location.href);

	let animationTitle = document.getElementById('animation-title').value;
	let animationDescription = document.getElementById('animation-desc').value;
	shapesList.setHtmlTextsForShapes();
	let data = JSON.stringify(shapesList, replacer);

	xhttp.open("POST", window.location.href, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.onreadystatechange = function () {
		if(xhttp.readyState === 4 && xhttp.status === 200) {
			let toastHTML = '<span style="color: rgb(131, 198, 134);">Saved Animation</span>';
			M.toast({html: toastHTML, displayLength: 1200});

			console.log('Saved animation on database');
		}
		if(xhttp.readyState === 4 && xhttp.status !== 200) {
			let toastHTML = '<span style="color: #ef5350">Saving Failed</span>';
			M.toast({html: toastHTML, displayLength: 1200});

			console.log("Can't Save animation on database");
		}
	};
	xhttp.send("title=" + animationTitle + "&description="+ animationDescription +"&data=" + data + "&thumbnail=" + thumbnail + "&duration=" + wholeDuration);


}

function loadAnimationData(data) {
	if(data){
		let potionData = JSON.parse(data).shapeList;

		let duration = JSON.parse(data).wholeDuration;
		wholeDuration = duration;
		if(document.getElementById('timespan')) document.getElementById('timespan').value = duration / 1000;
		setTimeGrid(wholeDuration);

		let backgroundColor = JSON.parse(data).backgroundColor;
		bgColor = backgroundColor;
		shapesList.backgroundColor = bgColor;
		document.getElementById('canvas').style.backgroundColor = bgColor;

		// console.log('Potion Data:');
		// console.log(potionData);
		
		if (potionData) {
			potionData.forEach(obj => {
				shapesList.loadObj(obj);
			});
			resetElementsList();
			shapesList.deselectAll();

			shapesList.updateShapeIds();
		}

		player.setAnimations();
		//shapesList.placeObjectsAndKeyframesAfterLoading();
	}
}

function updateWholeDuration(timeInput) {
	console.log("New duration: " + timeInput);
	let newTimeInSeconds = parseInt(timeInput);

	if (timeInput != null && timeInput != 0 && timeInput != "") //check if a Number was entered and the Number is not 0
	{
		//check if Value is in bounds and update the wholeDuration
		let inputElement = document.getElementById('timespan');
		let min = inputElement.getAttribute('min');
		let max = inputElement.getAttribute('max');
		//if in Bounds (min/max)
		if(checkForMinAndMax(newTimeInSeconds, min, max, inputElement))
		{
			wholeDuration = newTimeInSeconds * 1000;
		}
		else{
			console.log("Duration is too short or too long!");
			inputElement.value = wholeDuration / 1000; //if Input is wrong, keep the old value
		}
	}
	else {
		//if Input is wrong, keep the old value
		console.log("Wrong Time Input! - Keeping old Value")
		let inputElement = document.getElementById('timespan');
		inputElement.value = wholeDuration / 1000; //if Input is wrong, keep the old value
	}
	shapesList.wholeDuration = wholeDuration;

	setTimeGrid(wholeDuration);
	
}

function setTimeGrid(duration){
	let timelineGrid = document.getElementById('timeline-grid');
	let secondDistances = 100.0 / (duration /1000);
	let girdHTML = "";
	for(let i = 1; i < (duration/1000); i++ ){
		girdHTML += `<div style="position: absolute; width:1px; height:100%; left: ${i*secondDistances}%; z-index:10; background-color: #666666; opacity: 0.5"></div>`
	}
	timelineGrid.innerHTML = girdHTML;
	// console.log(girdHTML);
}

function freezeKeyframes(){
	//start with emptying the timeline
	frozenKeyframeArea = document.getElementById("frozenKeyframeArea");
	$(frozenKeyframeArea).empty();
	//get all visible Keyframes!
	let currentKeyframes = document.querySelectorAll( '.keyframe:not(.hidden)' );
	//let currentKeyframes = document.getElementsByClassName('keyframe');
	if(currentKeyframes.length > 0)
	{
		for(var i = 0; i<currentKeyframes.length; i++)
		{
			//clone the element
			let tmp = currentKeyframes[i].cloneNode(true);
			//change it's style/functionalities
			tmp.classList.remove('keyframe');
			tmp.classList.remove('selectedKeyframe');
			tmp.classList.add('frozenKeyframe');

			//add it to second keyframe area
			frozenKeyframeArea.appendChild(tmp);
		}
	}
	else
	{
		console.log("No keyframes to freeze!");
	}
  }