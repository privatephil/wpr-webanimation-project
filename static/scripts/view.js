// const ShapeList = require('/scripts/classes.js');

let shapesList = new ShapeList();
let keyframesManager = new KeyframesManager(shapesList);

let player = null;


/* _________  HELPERS  ___________ */

function getCanvas() {
	return document.getElementById('canvas');
}

// Converts from degrees to radians.
Math.radians = function (degrees) {
	return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = function (radians) {
	return radians * 180 / Math.PI;
};

// Tests for Touch Device
function isTouchDevice() {  
	try {  
	  document.createEvent("TouchEvent");  
	  return true;  
	} catch (e) {  
	  return false;  
	}  
  }

window.onresize = function(event) {
	setCanvasSize()
}

function setCanvasSize() {

	var ww = window.innerWidth;
	var wh = window.innerHeight;

	// Format Options
	let canvas_ratio = 16 / 9;

	// Fixed Height Settings
	var margin = 16;

	let container_height = (1 / canvas_ratio) * (ww);
	let canvas_height = container_height;
	let canvas_width = canvas_height * canvas_ratio;


	if (container_height > wh) {
		scale_factor = wh / container_height;
		canvas_height = canvas_height * scale_factor;
		canvas_width = canvas_width * scale_factor;
	}

	$('#canvas').css({
		'height': canvas_height + 'px',
		'width': canvas_width + 'px',
		'font-size': canvas_height + 'px',
	});
	$('#navigation').css({
		'height': canvas_height + 'px',
		'width': canvas_width + 'px',
		'font-size': canvas_height + 'px',
	});

}

function registerEventHandlers() {

	document.addEventListener('mousemove', ()=>{
		if(!isTouchDevice()){
			showNavigation();
		}
	})
	document.addEventListener('click', toggleNavigation)
	document.addEventListener('keydown', shortcuts);
	
	document.addEventListener('fullscreenchange', onFullScreenChange);
	document.addEventListener('webkitfullscreenchange', onFullScreenChange);
	document.addEventListener('mozfullscreenchange', onFullScreenChange);
	document.addEventListener('msfullscreenchange', onFullScreenChange);

	resetBindings();
}

function toggleNavigation() {
	let nav = document.getElementById('navigation');
	let allowedTargets = ['header', 'body', 'footer', 'canvas']
	console.log(event.target.id);
	console.log(nav.style.display);

	
	if(allowedTargets.includes(event.target.id))
	{
		if (nav.style.display === 'none') {
			showNavigation();
		}
		else{
			hideNavigation();
		}
	}
}

function showNavigation(){
	let nav = document.getElementById('navigation');
	nav.style.display= 'block';
	nav.style.pointerEvents= 'auto';
	nav.style.opacity = 1;
}

function hideNavigation(){
	let nav = document.getElementById('navigation');
	// nav.style.transition = 'opacity 0.5s';
	nav.style.opacity = 0;
	setTimeout( () => {
		let nav = document.getElementById('navigation');
		nav.style.display= 'none';
		nav.style.pointerEvents= 'none';
	}, 500)
}

window.onload = function () {
	registerEventHandlers();
	setCanvasSize()
	console.log('player init');
	player = new AnimationPlayer(shapesList);
	console.log(`Progess: ${player.progress}`);
	

	loadAnimationData(document.getElementById('potionDataHiddenInputText').value);
}

function canvasPixelPositionToPercent(x, y) {
	var canvasWidth = (parseFloat(getCanvas().style.width) || 0);
	var canvasHeight = (parseFloat(getCanvas().style.height) || 0);

	var positionInPercent = {
		x: 0,
		y: 0,
	}

	positionInPercent.x = (x / canvasWidth) * 100;
	positionInPercent.y = (y / canvasHeight) * 100;
	return positionInPercent;
}

/* Shortcuts */
function shortcuts(event) {
	let ctrlDown = event.ctrlKey
	let cmdDown = event.metaKey;
	let keyDown = event.key;
	console.log()
	// SPACEBAR -> Play Animation
	if(event.keyCode == 32){
		let playButton = document.getElementById('playButton');
		if(playButton.value == 0 || playButton.value == undefined){
			playButtonClick(event);
			hideNavigation()
		}
		else{
			pauseButtonClick(event);
			showNavigation()
		}
	}

}

/*
	GUI related
*/
function resetBindings() {
	//remove existing bindings
	$('.play-arrow').off('click');
	$('.pause-button').off('click');

	//set new bindings
	$('.play-arrow').bind('click', playButtonClick);
	$('.pause-button').bind('click', pauseButtonClick);

}

function playButtonClick(event) {
	//console.log('playButtonClick');
	var livePlayArrows = document.getElementsByClassName('play-arrow');

	for (var i = 0; i < livePlayArrows.length; i++) {
		livePlayArrows[i].innerHTML = 'pause';
		livePlayArrows[i].value = 1;
		livePlayArrows[i].classList.add('pause-button');
		livePlayArrows[i].classList.remove('play-arrow');
	}

	hideNavigation();
	resetBindings();
	startAnimation(event);
}

function pauseButtonClick(event) {
	//console.log('pauseButtonClick');
	var livePause = document.getElementsByClassName('pause-button');

	for (var i = 0; i < livePause.length; i++) {
		livePause[i].innerHTML = 'play_arrow';
		livePause[i].value = 0;
		livePause[i].classList.add('play-arrow');
		livePause[i].classList.remove('pause-button');
	}

	resetBindings();
	pauseAnimation(event);
}

function startAnimation(event) {

  shapesList.deselectAll();
  player.setAnimations();
  player.play();
}

function pauseAnimation(event) {
	//console.log('pauseAnimation')
	player.pause();
}

function jumpToStart(){
	var timeline = document.getElementById("timelinePointer");
	timeline.value = 0;
	player.gotoTime(timeline.value);
  }
  
  function jumpToEnd(){
	var timeline = document.getElementById("timelinePointer");
	timeline.value = 100;
	player.gotoTime(timeline.value);
  }
  
  function jumpForward(){ //Macht noch Probleme
	  var timeline = document.getElementById("timelinePointer");
	  var val = parseFloat(timeline.value) + 0.1;
	  timeline.value = val;
	  player.gotoTime(val);
  }
  
  function jumpBackward(){
	  var timeline = document.getElementById("timelinePointer");
	  var val = parseFloat(timeline.value) - 0.1;
	  timeline.value = val;
	  player.gotoTime(timeline.value);
  }

function loadAnimationData(data) {
	if(data){
		console.log(JSON.parse(data));
		
		let potionData = JSON.parse(data).shapeList;

		let duration = JSON.parse(data).wholeDuration;
		wholeDuration = duration;
		if(document.getElementById('timespan')) document.getElementById('timespan').value = duration / 1000;

		let backgroundColor = JSON.parse(data).backgroundColor;
		bgColor = backgroundColor;
		shapesList.backgroundColor = bgColor;
		document.getElementById('canvas').style.backgroundColor = bgColor;

		console.log('Potion Data:');
		console.log(potionData);
		
		if (potionData) {
			potionData.forEach(obj => {
				shapesList.loadObj(obj);
			});
			shapesList.deselectAll();

			shapesList.updateShapeIds();
		}

		player.setAnimations();

	}
}

function updateWholeDuration(timeInput) {
	console.log("New duration: " + timeInput);
	let newTimeInSeconds = parseInt(timeInput);

	if (timeInput != null && timeInput != 0 && timeInput != "") //check if a Number was entered and the Number is not 0
	{
		//check if Value is in bounds and update the wholeDuration
		let inputElement = document.getElementById('timespan');
		let min = inputElement.getAttribute('min');
		let max = inputElement.getAttribute('max');
		//if in Bounds (min/max)
		if(checkForMinAndMax(newTimeInSeconds, min, max, inputElement))
		{
			wholeDuration = newTimeInSeconds * 1000;
		}
		else{
			console.log("Duration is too short or too long!");
			inputElement.value = wholeDuration / 1000; //if Input is wrong, keep the old value
		}
	}
	else {
		//if Input is wrong, keep the old value
		console.log("Wrong Time Input! - Keeping old Value")
		let inputElement = document.getElementById('timespan');
		inputElement.value = wholeDuration / 1000; //if Input is wrong, keep the old value
	}
	shapesList.wholeDuration = wholeDuration;

	setTimeGrid(wholeDuration);
	
}

function onFullScreenChange(e)
  {
    let canvas = getCanvas();
    if(document.fullscreenElement)  // || document.fullscreenEnabled|| document.mozFullScreen
    {
      canvas.style.width = '100vw';
      canvas.style.height = '56.25vw';
    }
    else{
		setCanvasSize();
    }
  }


function goFullScreen(){
  var canvas = document.getElementById("canvas-container");

  if(!document.fullscreenElement){
    if(canvas.requestFullScreen)
        canvas.requestFullScreen();
    else if(canvas.webkitRequestFullScreen)
        canvas.webkitRequestFullScreen();
    else if(canvas.mozRequestFullScreen)
        canvas.mozRequestFullScreen();
  }
  else{
    if(document.exitFullscreen)
       document.exitFullscreen();
    else if(document.webkitExitFullscreen)
        document.webkitExitFullscreen();
    else if(document.mozCancelFullScreen)
        document.mozCancelFullScreen();
  }
}
