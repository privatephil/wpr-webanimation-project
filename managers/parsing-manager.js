let bodyParser = require('body-parser');

let parser;

const initParser = () => {
    parser = bodyParser.urlencoded({
        extended: true,
        limit: '50mb'
    });
}

const getParser = () => {
    return parser;
}

module.exports = {
    initParser,
    getParser
};