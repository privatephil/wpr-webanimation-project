let sqlite3 = require('sqlite3');
let database = 'potion-database'; // filepath o. ip-adress to database

/*
    Initialisation of database
*/
let db;

const initDB = () => {
    db = new sqlite3.Database(database);
}

const getDB = () => {
    return db;
}

module.exports = {
    initDB,
    getDB
};