// created with the help of: https://www.youtube.com/watch?v=OH6Z0dJ_Huk

let session = require('express-session');

let sess;

const initSession = () => {
  sess = session({
    name: 'sid',
    resave: false, //prevent storing sessions that were never modified
    saveUninitialized: false, //prevent storing of uninitialized (empty) sessions
    secret: 'secret',
    cookie: {
      maxAge: null,
      sameSite: true,
      secure: false //false only in production!
    }
  });
}

const getSession = () => {
  return sess;
}

module.exports = {
  initSession,
  getSession
};