var express = require('express');
var router = express.Router();

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// Models
let User = require('../models/user');
let Lesson = require('../models/lesson');

//Logged-In Check
let loginCheck = function (req, res, next) {
  console.log('loginCheck');

  let userId = req.session.userid;

  if (userId) {
    console.log('User is logged-in!');
    let user = new User();
    try {
      user.getById(userId, (row) => {
        user.id = row.UserId;
        user.username = row.Username;
        user.email = row.Email;
        user.password = row.Password;
        user.isAdmin = row.IsAdmin;

        res.locals.user = user;
        next();
      });
    } catch (e) {
      console.log(e);
    }
  } else {
    res.redirect('/');
  }
}

let checkPermission = function (req, res, next) {
  if (req.query.lessonId) {
    let lessonId = req.query.lessonId;

    let lessonGetter = new Lesson();
    let lesson = new Lesson();
    try {
      lessonGetter.getById(lessonId, (row) => {

        if (res.locals.user.id === row.CreatorId) {
          lesson.id = row.LessonId;
          lesson.title = row.Title;
          lesson.description = row.Description;
          lesson.date = row.Date;
          lesson.creatorId = row.CreatorId;

          res.locals.lesson = lesson;
          res.locals.lessonId = lessonId;

          next();
        } else {
          res.redirect('/');
        }
      });
    } catch (e) {
      console.log(e);
      res.redirect('/');
    }
  } else {
    res.redirect('/lessons');
  }
}

//Change Description for Lesson
function changeDesc(req, res, next) {

  let lesson = res.locals.lesson;

  if (req.body.title) {
    lesson.title = req.body.title;
  }

  if (req.body.desc) {
    lesson.description = req.body.desc;
  }

  lesson.update(function () {
    res.redirect('/lessons/' + req.query.lessonId);
  });
}

// change-desc
router.post('/', loginCheck, checkPermission, changeDesc);

module.exports = router;
