var express = require('express');
var router = express.Router();

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// Models
let User = require('../models/user');
let UserLesson = require('../models/userlesson');
let Lesson = require('../models/lesson');
let AnimationLesson = require('../models/animationlesson');

//Logged-In Check
let loginCheck = function (req, res, next) {
  console.log('loginCheck');

  let userId = req.session.userid;

  if (userId) {
    console.log('User is logged-in!');
    let user = new User();
    try {
      user.getById(userId, (row) => {
        user.id = row.UserId;
        user.username = row.Username;
        user.email = row.Email;
        user.password = row.Password;
        user.isAdmin = row.IsAdmin;

        res.locals.user = user;
        next();
      });
    } catch (e) {
      console.log(e);
      res.redirect('/');
    }
  } else {
    res.redirect('/');
  }
}

let retrieveLesson = function (req, res, next) {
  let user = res.locals.user;
  let lessonId = req.query.lessonId;

  let lesson = new Lesson();
  try {
    lesson.getById(lessonId, (row) => {
      lesson.id = row.LessonId;
      lesson.title = row.Title;
      lesson.description = row.Description;
      lesson.date = row.Date;
      lesson.creatorId = row.CreatorId;

      res.locals.lesson = lesson;

      if (user.id == lesson.creatorId) {
        next();
      } else {
        res.redirect('/');
      }
    });
  } catch (e) {
    console.log(e);
    res.redirect('/lessons');
  }
}

// Delete targeted Lesson
let deleteLesson = function (req, res, next) {
  let lesson = res.locals.lesson;

  try {
    lesson.remove(function () {
      console.log('lesson deleted');
      next();
    });


  } catch (e) {
    console.log(e);
    res.redirect('/admin');
  }

}

let deleteUserLessonEntry = function (req, res, next) {
  let lessonId = req.query.lessonId;

  //Delete UserLesson Entries of this User
  let userLesson = new UserLesson();

  try {
    userLesson.removeAllByLessonId(lessonId, function () {
      console.log('userlesson deleted');
      console.log('redirecting...');
      //res.redirect('/lessons');
      next();
    });
  } catch (e) {
    console.log(e);
    res.redirect('/lessons');
  }

}

let deleteAnimationLessonEntries = function (req, res, next) {
  console.log('deleteAnimationLessonEntries in delete-lesson')

  //Delete UserLesson Entries of this User
  let animationLesson = new AnimationLesson();

  try {
      animationLesson.removeAllByLessonId(req.query.lessonId, function () {
          console.log('animationlesson deleted');
          console.log('redirecting...');
          res.redirect('/lessons');
      });
  } catch (e) {
      console.log(e);
      res.redirect('/lessons');
  }

}

// lessons/id
router.get('/', loginCheck, retrieveLesson, deleteLesson, deleteUserLessonEntry, deleteAnimationLessonEntries);

module.exports = router;
