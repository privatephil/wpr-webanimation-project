var express = require('express');
var router = express.Router();

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Models
let User = require('../models/user');
let Animation = require('../models/animation');
let AnimationLesson = require('../models/animationlesson');
let Lesson = require('../models/lesson');


let getSpecificLesson = function (req, res) {
  console.log('getSpecificLesson');

  let lessonGetter = new Lesson();
  let lesson = new Lesson();
  try {
    lessonGetter.getByShare(req.query.lesson, (row) => {
      console.log('getByShare Lesson');

      lesson.id = row.LessonId;
      lesson.title = row.Title;
      lesson.description = row.Description;
      lesson.date = row.Date.substring(8, 10) + '.' + row.Date.substring(5, 7) + '.' + row.Date.substring(0, 4);
      lesson.creatorId = row.CreatorId;
      lesson.share = row.Share;

      res.locals.lesson = lesson;

      getAnimationsByLesson(req, res);
    });
  } catch (e) {
    console.log(e);
    res.redirect('/');
  }
}

let getAnimationsByLesson = function (req, res) {
  console.log('getAnimationsByLesson');
  let lesson = res.locals.lesson;

  res.locals.animations = [];

  let animationLessonGetter = new AnimationLesson();
  let animation;
  try {

    animationLessonGetter.getAnimationsByLessonId(lesson.id, (rows) => {
      rows.forEach(el => {
        animation = new Animation();

        animation.id = el.AnimationId;
        animation.title = el.Title;
        animation.description = el.Description;
        animation.date = el.Date.substring(8, 10) + '.' + el.Date.substring(5, 7) + '.' + el.Date.substring(0, 4);
        animation.duration = formatDuration(el.Duration / 1000);
        animation.creatorId = el.CreatorId;
        animation.share = el.Share;

        res.locals.animations.push(animation);
      });
      renderLesson(req, res);
    });
  } catch (e) {
    console.log(e);
    res.redirect('/');
  }
}

let renderAnimation = function (req, res) {
  console.log('loadAnimation');
  let animationGetter = new Animation();

  animationGetter.getByShare(req.query.animation, (row) => {
    res.render('view-animation-id', {
      title: row.Title,
      description: row.Description,
      duration: formatDuration(row.Duration / 1000),
      data: row.potionData
    });
  });
}

let renderLesson = function (req, res) {
  console.log('renderLesson');
  let lesson = res.locals.lesson;
  let animations = res.locals.animations;

  res.render('view-lesson-id', {
    //id: lesson.id,
    title: lesson.title,
    desc: lesson.description,
    //share: lesson.share,
    //date: lesson.date.substring(8,10)+ '.' + lesson.date.substring(5,7) + '.' + lesson.date.substring(0,4),
    animations: animations
  });
}

let renderView = function (req, res, next) {
  //User ID Ausgabe aus der Session
  console.log('renderView - animation or lesson?');

  // view?animation={id}
  if (typeof req.query.animation !== 'undefined') {
    console.log('is animation');
    console.log('animation share link: ' + req.query.animation);
    renderAnimation(req, res);
  }
  // view?lesson={id}
  else if (typeof req.query.lesson !== 'undefined') {
    console.log('is lesson');
    console.log('lesson share link: ' + req.query.lesson);
    getSpecificLesson(req, res);
  } else {
    res.send('undefined');
  }
}

function formatDuration(duration) {
  let minutes = Math.floor(duration / 60);
  if (minutes < 10) minutes = '0' + minutes;
  let seconds = duration % 60;
  if (seconds < 10) seconds = '0' + seconds;
  return minutes + ':' + seconds;
}

router.get('/', renderView);

module.exports = router;
