const express = require('express');
const router = express.Router();

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

router.get('/', function (req, res) {
  //Logged-In Check
  userId = req.session.userid;
  //User ID Ausgabe aus der Session
  console.log('User ID:' + req.session.userid + " logged out.");

  if (userId) {
    try {
      req.session.destroy();

      res.clearCookie('sid', { path: '/' }).status(200);
      res.redirect('/');
    } catch (e) {
      res.status(500);
      res.render('redirects/erroroccurred');
    }
  } else {
    res.redirect('/');
  }

});

module.exports = router;
