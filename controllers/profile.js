let express = require('express');
let router = express.Router();

// BCrypt
let bcrypt = require('bcrypt');

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Models
let User = require('../models/user');

//Logged-In Check
let loginCheck = function (req, res, next) {
  console.log('loginCheck');
  let userId = req.session.userid;

  if (userId) {
    console.log('User is logged-in!');
    let user = new User();
    try {
      user.getById(userId, (row) => {
        user.id = row.UserId;
        user.username = row.Username;
        user.email = row.Email;
        user.password = row.Password;
        user.isAdmin = row.IsAdmin;

        res.locals.user = user;
        next();
      });
    } catch (e) {
      console.log(e);
    }
  } else {
    res.redirect('/');
  }
}

let getUser = function (req, res, next) {

  userId = req.session.userid;

  try {
    let userGetter = new User();
    let user = new User();

    userGetter.getById(userId, (row) => {
      user.id = row.UserId;
      user.username = row.Username;
      user.email = row.Email;
      user.password = row.Password;
      user.isAdmin = row.IsAdmin;

      res.locals.user = user;

      next();
    });

  } catch (e) {
    console.log(e);
    res.redirect('/');
  }
}

let checkOldPassword = function (req, res, next) {

  let oldpassword = req.body.oldpw;

  if (res.locals.user.id !== 0 && res.locals.user.id === req.session.userid) {
    bcrypt.compare(oldpassword, res.locals.user.password, (err, rtn) => {
      if (err) {
        err.httpStatusCode = 500;
        return next(err);
      }

      if (rtn) {
        // Passwords match
        console.log("It's a match!");

        next();

      } else {
        // Passwords don't match
        console.log("Passwords don't match");

        res.status(500);
        res.render('redirects/erroroccurred');
      }
    });
  }
}

let updateUser = function (req, res, next) {
  let username = req.body.name;
  let newpassword = req.body.newpw;
  let rptpassword = req.body.rptpw;

  let user = res.locals.user;

  if (newpassword === rptpassword) {
    bcrypt.hash(newpassword, 10, function (err, hash) {
      if (err) {
        err.httpStatusCode = 500;
        return next(err);
      }

      if (hash) {
        // Passwords match
        console.log("It's a match!");

        user.username = username;
        user.password = hash;

        user.update(function () {
          next();
        });

      } else {
        // Passwords don't match
        console.log("Passwords don't match");

        res.status(500);
        res.render('redirects/erroroccurred');
      }

    });
  }
}

let loadProfilePage = function (req, res, next) {
  console.log('loadProfilePage');
  let user = res.locals.user;

  res.render('profile', {
    user: user
  });
}


router.get('/', loginCheck, loadProfilePage);

router.post('/', loginCheck, getUser, checkOldPassword, updateUser, loginCheck, loadProfilePage);

module.exports = router;
