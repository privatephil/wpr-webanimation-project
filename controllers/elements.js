var express = require('express');
var router = express.Router();
const multer = require('multer');
var path = require('path');
var fs = require('fs');
var svgo = require('svgo');

var elements = require('../models/elements');

// Multer Settings
var storage = multer.diskStorage({
    originalname: function (req, file, cb) {
        var name = file.originalname;
        cb(null, name);
    },
    destination: function (req, file, cb) {
        var filePath = path.join(__dirname, '../static/files/shapes');
        cb(null, filePath)
    },
    filename: function (req, file, cb) {
        //var filePath = path.join(__dirname, '../static/files/shapes');
        var randomString = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        //var fileName = file.fieldname + '-' + counter + path.extname(file.originalname);
        //counter++;
        var fileName = randomString + path.extname(file.originalname);
        cb(null, fileName);
    }
  })

var filter = function fileFilter (req, file, cb) {

    var ext = path.extname(file.originalname);
    var filePath = file.path;
    while(file.path){
        console.log(file.path);

    }
    
    if( ['.png', '.jpg', '.jpeg', '.svg', '.gif'].includes(ext) ){
  
        cb(null, true)
    }
    else{
        // To reject this file pass `false`, like so:
        cb(null, false)
    }
   
    // You can always pass an error if something goes wrong:
    // cb(new Error('I don\'t have a clue!'))
   
  }
   
var upload = multer({ storage: storage, fileFilter: filter })


// Session Management
// created with the help of: https://www.youtube.com/watch?v=OH6Z0dJ_Huk
var session = require('express-session');
router.use(session({
  name: 'sid',
  resave: false, //prevent storing sessions that were never modified
  saveUninitialized: false, //prevent storing of uninitialized (empty) sessions
  secret: 'secret',
  cookie: { 
    maxAge: null,
    sameSite: true,
    secure: false //false only in production!
  }
}));


// Upload Shapes
router.post('/shapes', upload.single('fileUpload'), (request, response, next) => {
    
    //Logged-In Check
    userId = request.session.userid;
    //User ID Ausgabe aus der Session
    console.log('User ID:' + request.session.userid);


    if(userId){
        const file = request.file;
        // console.log(request.file);
        if (!file) {
            const error = new Error('Please upload a file');
            error.httpStatusCode = 400;
            return next(error);
        }
        // if file is svg then setattribute preserveAspectRatio
        else{
            var ext = path.extname(file.originalname);
            if('.svg'.includes(ext))
            {
                var svg = new svgo({
                    plugins: [{
                        addAttributesToSVGElement: { attribute: 'preserveAspectRatio="none"'}
                    },{
                        removeHiddenElems: true
                    }]
                  });
        
                  fs.readFile(file.path, 'utf8', function(err, data) {
                    
                    if (err) {
                        return next(err);
                    }
    
                    svg.optimize(data, {path: file.path}).then(function(result) {
                
                        // console.log(result);
                        fs.writeFileSync(file.path, result.data);
                    });
                });
            }
        }

        // DB Entry - AnimationObject
        var fileName = file.filename;
        var category = request.body['category'];
        elements.CreateElement(file.originalname, '/files/shapes/' + fileName, category, function() {
            console.log('Shape Successfully Uploaded!')
            response.send();
        });

    } else {
        response.redirect('/');
    }    

    
  })


  // Get all Shapes by Category
  router.get('/shapes', (request, response, next) => {

        var category = request.get('category');
        elements.GetElements(category, (rows) => {
            response.setHeader('Content-Type', 'application/json');
            response.json(rows);
        })

  })

  // Delete Shape by ID
  router.delete('/shapes', (request, response) => {
    id = request.get('id');
    if(id){
        elements.GetElement(id, function (row) {
            // filePath on OS
            var filePath = path.join(__dirname, '../static' + row["ObjectFile"]);

            // Delete File from OS
            if(fs.existsSync(filePath)){
                fs.unlinkSync(filePath);
                console.log(`Shape(${id}) Sucessfully deleted from OS: \n ${filePath}`);
            }
            else{
                console.error(`Couldn't delete Shape(${id}) from OS! \n     Path does not exist:  ${filePath}`);
            }
            
            // Delete File from DB
            elements.DeleteElement(id, function() {
                console.log(`Shape(${id}) Sucessfully deleted from DB`);
                response.send(`Shape(${id}) Sucessfully deleted from DB`);
            })

        }) 
    }
    else{
        response.status('404').send('Sorry cant find that');
    }

})

  // Get all Shapes by Category
  router.get('/icons', (request, response, next) => {

    elements.GetIcons( (iconFolders) => {
        response.setHeader('Content-Type', 'application/json');
        response.json(iconFolders);
    })

})

module.exports = router;
