var express = require('express');
var router = express.Router();

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// Models
let User = require('../models/user');


//Logged-In Check
let loginCheck = function (req, res, next) {
  let userId = req.session.userid;

  if (userId && userId == req.query.userId) {
    let user = new User();
    try {
      user.getById(userId, (row) => {
        user.id = row.UserId;
        user.username = row.Username;
        user.email = row.Email;
        user.password = row.Password;
        user.isAdmin = row.IsAdmin;

        res.locals.user = user;
        next();
      });
    } catch (e) {
      console.log(e);
    }
  } else {
    res.redirect('/');
  }
}

// Delete Account of this User
function deleteAccount(req, res, next) {
  let user = res.locals.user;
  try {
    user.remove(function () {
      req.session.destroy();

      res.clearCookie('sid', { path: '/' }).status(200);
      res.redirect('/');
    });


  } catch (e) {
    res.status(500);
    res.render('redirects/erroroccurred');
  }
}

router.get('/', loginCheck, deleteAccount);

module.exports = router;
