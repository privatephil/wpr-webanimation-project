let express = require('express');
let router = express.Router();

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// BCrypt
let bcrypt = require('bcrypt');

// Models
let User = require('../models/user');

function emailExistsCheck(req, res, next) {
  console.log("emailExistsCheck");

  let email = req.body.email;
  console.log(email);
  if (email) {
    let userGetter = new User();
    userGetter.getByEmail(email, (row) => {
      if (row) {
        console.log('E-Mail already exists!');
        res.redirect('/');
      } else {
        console.log('E-Mail not registered yet!');
        next();
      }
    });
  }
}

// Only register if the Check from emailExistsCheck allows
function register(req, res, next) {
  console.log("register");
  let username = req.body.username;
  let password = req.body.password;
  let email = req.body.email;
  let emailrepeat = req.body.emailrepeat;

  if (email === emailrepeat) {
    console.log("email equals repeated");
    try {
      let user = new User();

      bcrypt.hash(password, 10, function (err, hash) {
        console.log("hashed!");
        user.username = username;
        user.password = hash;
        user.email = email;

        user.create(function () {
          //TODO: hier sollte jetzt das Feedback für den Nutzer ersichtlich werden, dass die Registrierung funktioniert hat
          //evtl brauchen wir eine javascript methode im frontend die dann ein popup triggert, oder wir rendern es von hier aus
          //in den nachfolgenden view. Der Nutzer sollte sich jetzt auf jeden Fall neu anmelden, er wird nicht automatisch angemeldet.
          console.log('Registration succesful!');

          res.redirect('/');
        });
      });

    } catch (err) {
      console.log(err.message);
      res.redirect('/');
    }

  } else {
    console.log('E-Mail and Repeat not the same!');
    res.redirect('/');
  }
}


router.post('/', emailExistsCheck, register);

module.exports = router;
