let express = require('express');
let router = express.Router();

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Models
let User = require('../models/user');
let Lesson = require('../models/lesson');
let UserLesson = require('../models/userlesson');
let AnimationLesson = require('../models/animationlesson');
let Animation = require('../models/animation');


//Logged-In Check
let loginCheck = function (req, res, next) {
    console.log('loginCheck');
    let userId = req.session.userid;

    if (userId) {
        console.log('User is logged-in!');
        let user = new User();

        try {
            user.getById(userId, (row) => {
                user.id = row.UserId;
                user.username = row.Username;
                user.email = row.Email;
                user.password = row.Password;
                user.isAdmin = row.IsAdmin;

                res.locals.user = user;

                next();
            });
        } catch (e) {
            console.log(e);
            res.redirect('/');
        }
    } else {
        res.redirect('/');
    }
}

//Get Lesson Infos to all Lessons
let getLessons = function (req, res, next) {
    console.log('getLessons');

    userId = req.session.userid;

    res.locals.lessons = [];

    let search;
    let sort;
    if (req.query.search) {
        console.log(req.query.search);
        search = req.query.search;

        res.locals.searchValue = search;
    }
    if (req.query.sort) {
        console.log(req.query.sort);
        sort = req.query.sort;
    }

    let lessonGetter = new Lesson();
    let lesson;
    try {
        if (!search && !sort) {
            console.log("search oder sort nicht gegeben!");
            lessonGetter.getByCreatorId(userId, (rows) => {
                rows.forEach(el => {
                    lesson = new Lesson();

                    lesson.id = el.LessonId;
                    lesson.title = el.Title;
                    lesson.description = el.Description;
                    lesson.date = el.Date.substring(8,10)+ '.' + el.Date.substring(5,7) + '.' + el.Date.substring(0,4);
                    lesson.creatorId = el.CreatorId;
                    lesson.share = el.Share;

                    res.locals.lessons.push(lesson);
                });
                next();
            });
        } else {
            console.log("search oder sort gegeben!");
            lessonGetter.getByCreatorIdSearchSort(userId, search, sort, (rows) => {
                rows.forEach(el => {
                    lesson = new Lesson();

                    lesson.id = el.AnimationId;
                    lesson.title = el.Title;
                    lesson.description = el.Description;
                    lesson.date = el.Date.substring(8,10)+ '.' + el.Date.substring(5,7) + '.' + el.Date.substring(0,4);
                    lesson.creatorId = el.CreatorId;

                    res.locals.lessons.push(lesson);
                });
                next();
            });
        }
    } catch (e) {
        console.log(e);
        res.redirect('/');
    }
}

let createNewLessonOnDatabase = function (req, res, next) {
    console.log('createNewLessonOnDatabase');

    let title = 'New Lesson'
    let description = 'New Description'

    let userId = req.session.userid;
    let lesson = new Lesson(title, description, userId);

    lesson.create(function () {
        lesson.getLatestByCreatorId(userId, function (row) {
            if (row) {
                try {
                    let userLesson = new UserLesson(userId, row.LessonId);
                    userLesson.create(function () {
                        res.redirect('/lessons/' + row.LessonId);
                    });

                } catch (e) {
                    console.log(e);
                    res.redirect('/');
                }

            } else {
                res.redirect('/');
            }
        });
    });
}

let loadManageLessonsPage = function (req, res, next) {
    console.log('loadManageLessonsPage');

    let user = res.locals.user;
    let placeholder;

    if (res.locals.searchValue) {
        placeholder = res.locals.searchValue;
    }

    console.log('render lessons');
    res.render('lessons', {
        lessons: res.locals.lessons,
        listType: 'lessons',
        user: user,
        placeholder: placeholder
    });
}

let getSpecificLesson = function (req, res, next) {
    console.log('getSpecificLesson');
    let lessonId = req.params.id;

    let lessonGetter = new Lesson();
    let lesson = new Lesson();;
    try {
        lessonGetter.getById(lessonId, (row) => {
            console.log('getById in getLesson');

            lesson.id = row.LessonId;
            lesson.title = row.Title;
            lesson.description = row.Description;
            lesson.date = row.Date.substring(8,10)+ '.' + row.Date.substring(5,7) + '.' + row.Date.substring(0,4);
            lesson.creatorId = row.CreatorId;
            lesson.share = row.Share;

            res.locals.lesson = lesson;

            next();
        });
    } catch (e) {
        console.log(e);
        res.redirect('/');
    }
}

let getAnimationsByLesson = function (req, res, next) {
    console.log('getAnimationsByLesson');
    let lesson = res.locals.lesson;

    res.locals.animations = [];

    let animationLessonGetter = new AnimationLesson();
    let animation;
    try {

        animationLessonGetter.getAnimationsByLessonId(lesson.id, (rows) => {
            rows.forEach(el => {
                animation = new Animation();

                animation.id = el.AnimationId;
                animation.title = el.Title;
                animation.description = el.Description;
                animation.date = el.Date.substring(8,10)+ '.' + el.Date.substring(5,7) + '.' + el.Date.substring(0,4);
                animation.duration = formatDuration(el.Duration / 1000);
                animation.creatorId = el.CreatorId;
                animation.share = el.Share;

                res.locals.animations.push(animation);
            });
            next();
        });
    } catch (e) {
        console.log(e);
        res.redirect('/');
    }
}




let getAnimationsToAdd = function (req, res, next) {
    // Hier müssen die Animationen, die bereits in der Lesson sind nicht übernommen werden.
    console.log('getAnimationsToAdd');
    let animationGetter = new Animation();
    let animation;

    res.locals.animationsToAdd = [];
    try {
        console.log('whole animationsToAdd')
        animationGetter.getByCreatorNotInLesson(res.locals.user.id, res.locals.lesson.id, (rows) => {
            rows.forEach(el => {

                console.log("Id: " + el.AnimationId + ", Title: " + el.Title);
                animation = new Animation();
                animation.id = el.AnimationId;
                animation.title = el.Title;
                animation.description = el.Description;
                animation.date = el.Date.substring(8,10)+ '.' + el.Date.substring(5,7) + '.' + el.Date.substring(0,4);
                animation.duration = formatDuration(el.Duration / 1000);
                animation.creatorId = el.CreatorId;

                res.locals.animationsToAdd.push(animation);
            });
            next();
        });
    } catch (e) {
        console.log(e);
        res.redirect('/');
    }
}




let renderLesson = function (req, res, next) {

    let lesson = res.locals.lesson;
    let animations = res.locals.animations;
    let user = res.locals.user;
    let animationsToAdd = res.locals.animationsToAdd;

    console.log("und nochmal: ");
    console.log(animationsToAdd);

    console.log('renderLesson');
    res.render('manage-lesson', {
        id: lesson.id,
        title: lesson.title,
        desc: lesson.description,
        share: lesson.share,
        date: lesson.date.substring(8,10)+ '.' + lesson.date.substring(5,7) + '.' + lesson.date.substring(0,4),
        listType: 'manageLesson',
        animations: animations,
        user: user,
        animationsToAdd: animationsToAdd,
    });
}

function formatDuration(duration)
{
    let minutes = Math.floor(duration / 60);
    if (minutes < 10) minutes = '0' + minutes;
    let seconds = duration % 60;
    if (seconds < 10) seconds = '0' + seconds;
    return minutes + ':' + seconds;
}

/* Creates a new lesson */
router.post('/', loginCheck, createNewLessonOnDatabase);

/* Returns lesson */
router.get('/', loginCheck, getLessons, loadManageLessonsPage);
router.get('/:id', loginCheck, getSpecificLesson, getAnimationsToAdd, getAnimationsByLesson, renderLesson);

module.exports = router;