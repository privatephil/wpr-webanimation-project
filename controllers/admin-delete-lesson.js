const express = require('express');
const router = express.Router();

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Models
let User = require('../models/user');
let UserLesson = require('../models/userlesson');
let Lesson = require('../models/lesson');
let AnimationLesson = require('../models/animationlesson');

//Logged-In and  Admin Check
let adminLoginCheck = function (req, res, next) {
  console.log('adminLoginCheck in admin-delete-lesson');
  let adminId = req.session.userid;
  let lessonId = req.query.lessonId;

  console.log('adminId: ' + adminId);
  console.log('lessonId: ' + adminId);
  console.log('req.session.isAdmin: ' + req.session.isAdmin);

  if (adminId && req.session.isAdmin == 1 && lessonId) {
    console.log('is an Admin');
    res.locals.lessonId = lessonId;
    next();
  } else {
    res.redirect('/');
  }
}

// Delete targeted Lesson
let deleteLesson = function (req, res, next) {
  console.log('delete lesson in admin-delete-lesson');
  let lessonId = res.locals.lessonId;

  //Delete the Lesson from Database
  let lesson = new Lesson();
  try {
    lesson.getById(lessonId, (row) => {
      lesson.id = row.LessonId;
      lesson.title = row.Title;
      lesson.description = row.Description;
      lesson.date = row.Date;
      lesson.creatorId = row.CreatorId;

      console.log(lesson);

      // remove user from database
      lesson.remove(function () {
        console.log('lesson deleted');
        next();
      });
    });

  } catch (e) {
    console.log(e);
    res.redirect('/admin');
  }

}

let deleteUserLessonEntry = function (req, res, next) {
  console.log('deleteUserLessonEntry in admin-delete-lesson');

  //Delete UserLesson Entries of this User
  let userLesson = new UserLesson();

  try {
    userLesson.removeAllByLessonId(res.locals.lessonId, function () {
      console.log('userlesson deleted');
      console.log('redirecting...');
      //res.redirect('/admin');
      next();
    });
  } catch (e) {
    console.log(e);
    res.redirect('/admin');
  }

}

let deleteAnimationLessonEntries = function (req, res, next) {
  console.log('deleteAnimationLessonEntries in delete-lesson')

  //Delete UserLesson Entries of this User
  let animationLesson = new AnimationLesson();

  try {
      animationLesson.removeAllByLessonId(res.locals.lessonId, function () {
          console.log('animationlesson deleted');
          console.log('redirecting...');
          res.redirect('/admin');
      });
  } catch (e) {
      console.log(e);
      res.redirect('/admin');
  }

}

// Delete a Lesson as Admin
router.get('/', adminLoginCheck, deleteLesson, deleteUserLessonEntry, deleteAnimationLessonEntries);

module.exports = router;
