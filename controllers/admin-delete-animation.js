const express = require('express');
const router = express.Router();

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Models
let User = require('../models/user');
let Animation = require('../models/animation');
let AnimationLesson = require('../models/animationlesson');

//Logged-In and  Admin Check
let adminLoginCheck = function (req, res, next) {
    console.log('adminLoginCheck in admin-delete-animation');
    let adminId = req.session.userid;
    let animationId = req.query.animationId;

    console.log('adminId: ' + adminId);
    console.log('animationId: ' + animationId);
    console.log('req.session.isAdmin: ' + req.session.isAdmin);

    if (adminId && req.session.isAdmin == 1 && animationId) {
        console.log('is an Admin');
        res.locals.animationId = animationId;
        next();
    } else {
        res.redirect('/');
    }
}

// Delete targeted Lesson
let deleteAnimation = function (req, res, next) {
    console.log('deleteAnimation in admin-delete-animation')
    let animationId = res.locals.animationId;

    //Delete the Lesson from Database
    let animation = new Animation();
    try {
        animation.getById(animationId, (row) => {
            animation.id = row.AnimationId;
            animation.title = row.Title;
            animation.description = row.Description;
            animation.date = row.Date;
            animation.creatorId = row.CreatorId;

            // console.log(animation);

            // remove user from database
            animation.remove(function () {
                console.log('animation deleted');
                next();
            });
        });

    } catch (e) {
        console.log(e);
        res.redirect('/admin');
    }

}

let deleteAnimationLessonEntry = function (req, res, next) {
    console.log('deleteAnimationLessonEntry in admin-delete-animation')

    //Delete UserLesson Entries of this User
    let animationLesson = new AnimationLesson();

    try {
        animationLesson.removeAllByAnimationId(res.locals.animationId, function () {
            console.log('animationlesson deleted');
            console.log('redirecting...');
            res.redirect('/admin');
        });
    } catch (e) {
        console.log(e);
        res.redirect('/admin');
    }

}

// Delete a Lesson as Admin
router.get('/', adminLoginCheck, deleteAnimation, deleteAnimationLessonEntry);

module.exports = router;
