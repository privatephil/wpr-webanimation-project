
const express = require('express');
const router = express.Router();

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Models
let User = require('../models/user');
let Lesson = require('../models/lesson');
let Animation = require('../models/animation');

//Logged-In Check
let loginCheck = function (req, res, next) {
    console.log('loginCheck');

    let userId = req.session.userid;

    if (userId) {
        console.log('User is logged-in!');
        let user = new User();
        try {
            user.getById(userId, (row) => {
                user.id = row.UserId;
                user.username = row.Username;
                user.email = row.Email;
                user.password = row.Password;
                user.isAdmin = row.IsAdmin;

                res.locals.user = user;
                next();
            });
        } catch (e) {
            console.log(e);
        }
    } else {
        res.redirect('/');
    }
}

//Admin Check
let adminCheck = function (req, res, next) {
    console.log('adminLoginCheck');
    console.log('UserID: ' + req.session.userid);

    let isAdmin = res.locals.user.isAdmin;

    if (isAdmin === 1) {
        next();
    } else {
        res.redirect('/');
    }
}

//Get Users
let getUsers = function (req, res, next) {
    console.log('getUsers');

    res.locals.users = [];
    let userGetter = new User();
    let user;
    try {
        userGetter.getAll((rows) => {
            rows.forEach(el => {
                user = new User();

                user.id = el.UserId;
                user.username = el.Username;
                user.email = el.Email;
                user.password = el.Password;
                user.isAdmin = el.IsAdmin;

                res.locals.users.push(user);
            });
            next();
        });
    } catch (e) {
        console.log(e);
        next();
    }

}

//Get Lessons
let getLessons = function (req, res, next) {
    console.log('getLessons');

    res.locals.lessons = [];

    let lessonGetter = new Lesson();
    let lesson;
    if (!req.query.userid) {
        try {
            lessonGetter.getAll((rows) => {
                rows.forEach(el => {
                    lesson = new Lesson();

                    lesson.id = el.LessonId;
                    lesson.title = el.Title;
                    lesson.description = el.Description;
                    lesson.date = el.Date;
                    lesson.creatorId = el.CreatorId;
                    lesson.share = el.Share;

                    res.locals.lessons.push(lesson);
                });
                next();
            });
        } catch (e) {
            console.log(e);
            next();
        }
    } else {
        try {
            lessonGetter.getByCreatorId(req.query.userid, (rows) => {
                rows.forEach(el => {
                    lesson = new Lesson();

                    lesson.id = el.LessonId;
                    lesson.title = el.Title;
                    lesson.description = el.Description;
                    lesson.date = el.Date;
                    lesson.creatorId = el.CreatorId;
                    lesson.share = el.Share;

                    res.locals.lessons.push(lesson);
                });
                next();
            });
        } catch (e) {
            console.log(e);
            next();
        }
    }
}

//Get Animations
let getAnimations = function (req, res, next) {
    console.log('getAnimations');

    res.locals.animations = [];

    let animationGetter = new Animation();
    let animation;
    if (!req.query.userid) {
        try {
            animationGetter.getAll((rows) => {
                rows.forEach(el => {
                    animation = new Animation();

                    animation.id = el.AnimationId;
                    animation.title = el.Title;
                    animation.description = el.Description;
                    animation.date = el.Date;
                    animation.creatorId = el.CreatorId;
                    animation.share = el.Share;
                    animation.duration = formatDuration(el.Duration / 1000);

                    res.locals.animations.push(animation);
                });
                next();
            });
        } catch (e) {
            console.log(e);
            next();
        }
    } else {
        try {
            animationGetter.getByCreatorId(req.query.userid, (rows) => {
                rows.forEach(el => {
                    animation = new Animation();

                    animation.id = el.AnimationId;
                    animation.title = el.Title;
                    animation.description = el.Description;
                    animation.date = el.Date;
                    animation.creatorId = el.CreatorId;
                    animation.share = el.Share;
                    animation.duration = formatDuration(el.Duration / 1000);

                    res.locals.animations.push(animation);
                });
                next();
            });
        } catch (e) {
            console.log(e);
            next();
        }

    }
}

//Load Admin Page with all Lists
let loadAdminPage = function (req, res, next) {
    console.log('loadAdminPage');

    let user = res.locals.user;
    let query = "x";
    if (req.query.userid) {
        query = req.query.userid;
    }

    res.render('admin', {
        users: res.locals.users,
        lessons: res.locals.lessons,
        animations: res.locals.animations,
        user: user,
        query: query

    });
}

function formatDuration(duration)
{
    let minutes = Math.floor(duration / 60);
    if (minutes < 10) minutes = '0' + minutes;
    let seconds = duration % 60;
    if (seconds < 10) seconds = '0' + seconds;
    return minutes + ':' + seconds;
}

router.get('/', loginCheck, adminCheck, getUsers, getLessons, getAnimations, loadAdminPage);

module.exports = router;
