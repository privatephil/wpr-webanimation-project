var express = require('express');
var router = express.Router();

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// Models
let User = require('../models/user');
let Animation = require('../models/animation');
let AnimationLesson = require('../models/animationlesson');

//Logged-In Check
let loginCheck = function (req, res, next) {
  console.log('loginCheck');

  let userId = req.session.userid;

  if (userId) {
    console.log('User is logged-in!');
    let user = new User();
    try {
      user.getById(userId, (row) => {
        user.id = row.UserId;
        user.username = row.Username;
        user.email = row.Email;
        user.password = row.Password;
        user.isAdmin = row.IsAdmin;

        res.locals.user = user;
        next();
      });
    } catch (e) {
      console.log(e);
      res.redirect('/');
    }
  } else {
    res.redirect('/');
  }
}

//Owner Check
let retrieveAnimation = function (req, res, next) {
  let user = res.locals.user;
  let animationId = req.query.animationId;

  let animation = new Animation();
  try {
    animation.getById(animationId, (row) => {
      animation.id = row.AnimationId;
      animation.title = row.Title;
      animation.description = row.Description;
      animation.date = row.Date;
      animation.creatorId = row.CreatorId;

      res.locals.animation = animation;

      if (user.id == animation.creatorId) {
        next();
      } else {
        res.redirect('/');
      }
    });
  } catch (e) {
    console.log(e);
    res.redirect('/animations');
  }
}

// Delete targeted Lesson
let deleteAnimation = function (req, res, next) {
  let animation = res.locals.animation;

  try {
    animation.remove(function () {
      console.log('animation deleted');
      next();
    });


  } catch (e) {
    console.log(e);
    res.redirect('/animations');
  }

}

let deleteAnimationLessonEntry = function (req, res, next) {
  let animationId = req.query.animationId;

  //Delete UserLesson Entries of this User
  let animationLesson = new AnimationLesson();

  try {
    animationLesson.removeAllByAnimationId(animationId, function () {
      console.log('animationlesson deleted');
      console.log('redirecting...');
      res.redirect('/animations');
    });
  } catch (e) {
    console.log(e);
    res.redirect('/animations');
  }

}

// lessons/id
router.get('/', loginCheck, retrieveAnimation, deleteAnimation, deleteAnimationLessonEntry);

module.exports = router;
