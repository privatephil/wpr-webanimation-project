var express = require('express');
var router = express.Router();

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// Models
let User = require('../models/user');
let Lesson = require('../models/lesson');
let AnimationLesson = require('../models/animationlesson');
let Animation = require('../models/animation');

//Logged-In Check
let loginCheck = function (req, res, next) {
  console.log('loginCheck');

  let userId = req.session.userid;

  if (userId) {
    console.log('User is logged-in!');
    let user = new User();
    try {
      user.getById(userId, (row) => {
        user.id = row.UserId;
        user.username = row.Username;
        user.email = row.Email;
        user.password = row.Password;
        user.isAdmin = row.IsAdmin;

        res.locals.user = user;
        next();
      });
    } catch (e) {
      console.log(e);
      res.redirect('/');
    }
  } else {
    res.redirect('/');
  }
}

let checkPermission = function (req, res, next) {
  if (req.query.lessonId && req.query.animationId) {
    let lessonId = req.query.lessonId;
    let animationId = req.query.animationId;

    let lessonGetter = new Lesson();
    let lesson = new Lesson();
    try {
      lessonGetter.getById(lessonId, (row) => {

        if (res.locals.user.id === row.CreatorId) {
          lesson.id = row.LessonId;
          lesson.title = row.Title;
          lesson.description = row.Description;
          lesson.date = row.Date;
          lesson.creatorId = row.CreatorId;

          res.locals.lesson = lesson;
          res.locals.lessonId = lessonId;
          res.locals.animationId = animationId;

          next();
        } else {
          res.redirect('/');
        }
      });
    } catch (e) {
      console.log(e);
      res.redirect('/');
    }
  } else {
    res.redirect('/lessons');
  }
}

let checkAnimation = function (req, res, next) {
  let animationGetter = new Animation();

  try {
    animationGetter.getById(res.locals.animationId, (row) => {
      if (row.AnimationId) {
        next();
      } else {
        res.redirect('/');
      }
    });
  } catch (e) {
    console.log(e);
    res.redirect('/');
  }
}


//Add Animation to Lesson
let addAnimationToLesson = function (req, res, next) {
  try {
    let animationLesson = new AnimationLesson(res.locals.lessonId, res.locals.animationId);

    animationLesson.create(function () {
      res.redirect('/lessons/' + res.locals.lessonId);
    });
  } catch (e) {
    console.log(e);
    res.redirect('/lessons')
  }

}

router.post('/', loginCheck, checkPermission, checkAnimation, addAnimationToLesson);

module.exports = router;
