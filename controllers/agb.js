var express = require('express');
var router = express.Router();

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Models
let User = require('../models/user');


//Logged-In Check
let loginCheck = function (req, res, next) {
    console.log('loginCheck');
    let userId = req.session.userid;

    if (userId) {
        console.log('User is logged-in!');
        let user = new User();
        try {
            user.getById(userId, (row) => {
                user.id = row.UserId;
                user.username = row.Username;
                user.email = row.Email;
                user.password = row.Password;
                user.isAdmin = row.IsAdmin;

                res.locals.user = user;
                next();
            });
        } catch (e) {
            console.log(e);
        }
    } else {
        res.render('sign-in');
    }
}

let loadAGBPage = function (req, res, next) {
    console.log('LoadAGBPage ');

    let user = res.locals.user;

    res.render('agb', {
        user: user
    });
}

/* Start Page */
router.get('/', loginCheck, loadAGBPage);

module.exports = router;