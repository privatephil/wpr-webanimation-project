const express = require('express');
const router = express.Router();

// File system
let fs = require('fs');

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// Models
let User = require('../models/user');
let Animation = require('../models/animation');

//Logged-In Check
let loginCheck = function (req, res, next) {
    console.log('loginCheck');

    let userId = req.session.userid;

    if (userId) {
        console.log('User is logged-in!');
        let user = new User();
        try {
            user.getById(userId, (row) => {
                user.id = row.UserId;
                user.username = row.Username;
                user.email = row.Email;
                user.password = row.Password;
                user.isAdmin = row.IsAdmin;

                res.locals.user = user;
                next();
            });
        } catch (e) {
            console.log(e);
            res.redirect('/');
        }
    } else {
        res.redirect('/');
    }
}

// Create a new Animation
let createNewAnimationOnDatabase = function (req, res, next) {
    console.log('createNewAnimationOnDatabase');

    let title = 'New Title';
    let description = 'New Description';
    let duration = 5000; // standard duration in milliseconds

    let userId = req.session.userid;
    let animation = new Animation(title, description, userId, duration);

    animation.create(function () {
        animation.getLatestByCreatorId(userId, function (row) {
            if (row) {
                try {
                    res.redirect('/animations/' + row.AnimationId);
                } catch (e) {
                    console.log(e);
                    res.redirect('/');
                }

            } else {
                res.redirect('/');
            }
        });
    });

}

let saveAnimationChangesOnDatabase = function (req, res, next) {
    console.log('saveAnimationChangesOnDatabase');

    let title = req.body.title;
    let description = req.body.description;
    let thumbnail = req.body.thumbnail;
    let duration = req.body.duration;

    console.log(title + ', ' + description);

    let userId = req.session.userid;
    let animationId = req.params.id;
    let animation = new Animation();

    animation.getById(animationId, function (row) {
        console.log('got my animation from db');
        animation.id = row.AnimationId;
        animation.title = title;
        animation.description = description;
        animation.creatorId = row.CreatorId;
        //animation.thumbnail = thumbnail;
        animation.data = req.body.data;
        animation.duration = duration;

        // is current user the creator/owner of this animation?
        if (userId === row.CreatorId) {
            console.log("updating animation on database");
            try {
                animation.update(function () {

                    let data = thumbnail.slice(thumbnail.indexOf(',') + 1).replace(/\s/g, '+');
                    let buf = Buffer.from(data, 'base64');
                    fs.writeFile('static/files/thumbnails/' + animation.id + '.png', buf, function (error) {
                        if (error) {
                            console.log(error);
                        }
                        res.redirect('/animations/' + animation.id);
                    });

                });
            } catch (e) {
                console.log(e);
                res.redirect('/');
            }

        } else {
            res.redirect('/');
        }

    });

}

//Get Animations of this User
let getAnimations = function (req, res, next) {
    console.log('getAnimations');

    userId = req.session.userid;

    res.locals.animations = [];

    let search;
    let sort;
    if (req.query.search) {
        console.log(req.query.search);
        search = req.query.search;

        res.locals.searchValue = search;
    }
    if (req.query.sort) {
        console.log(req.query.sort);
        sort = req.query.sort;
    }

    let animationGetter = new Animation();
    let animation;
    try {
        if (!search && !sort) {
            console.log("search oder sort nicht gegeben!");
            animationGetter.getByCreatorId(userId, (rows) => {
                rows.forEach(el => {
                    animation = new Animation();

                    animation.id = el.AnimationId;
                    animation.title = el.Title;
                    animation.description = el.Description;
                    animation.date = el.Date.substring(8, 10) + '.' + el.Date.substring(5, 7) + '.' + el.Date.substring(0, 4);
                    animation.duration = formatDuration(el.Duration / 1000);
                    animation.potionData = el.potionData;
                    animation.creatorId = el.CreatorId;
                    //animation.thumbnail = el.Thumbnail;
                    animation.share = el.Share;

                    res.locals.animations.push(animation);
                });
                next();
            });
        } else {
            console.log("search oder sort gegeben!");
            animationGetter.getByCreatorIdSearchSort(userId, search, sort, (rows) => {
                rows.forEach(el => {
                    animation = new Animation();

                    animation.id = el.AnimationId;
                    animation.title = el.Title;
                    animation.description = el.Description;
                    animation.date = el.Date.substring(8, 10) + '.' + el.Date.substring(5, 7) + '.' + el.Date.substring(0, 4);
                    animation.duration = formatDuration(el.Duration / 1000);
                    animation.potionData = el.potionData;
                    animation.creatorId = el.CreatorId;
                    //animation.thumbnail = el.Thumbnail;
                    animation.share = el.Share;

                    res.locals.animations.push(animation);
                });
                next();
            });
        }
    } catch (e) {
        console.log(e);
        res.redirect('/');
    }
}

let loadAnimationsPage = function (req, res, next) {
    console.log('loadAnimationsPage');

    let user = res.locals.user;
    let placeholder;

    if (res.locals.searchValue) {
        placeholder = res.locals.searchValue;
    }

    console.log("render manage-animations");
    res.render('manage-animations', {
        animations: res.locals.animations,
        listType: 'animations',
        user: user,
        placeholder: placeholder
    });
}

let loadEditorPage = function (req, res, next) {
    console.log('loadEditorPage');

    let user = res.locals.user;
    let animationGetter = new Animation();

    animationGetter.getById(req.params.id, (row) => {
        if (row.CreatorId === user.id) {
            res.render('editor', {
                title: row.Title,
                description: row.Description,
                data: row.potionData,
                //thumbnail: row.Thumbnail,
                user: user
            });
        } else {
            res.redirect('/');
        }
    });
}

function formatDuration(duration) {
    let minutes = Math.floor(duration / 60);
    if (minutes < 10) minutes = '0' + minutes;
    let seconds = duration % 60;
    if (seconds < 10) seconds = '0' + seconds;
    return minutes + ':' + seconds;
}

/* Creates a new animation */
router.post('/', loginCheck, createNewAnimationOnDatabase);
router.post('/:id', loginCheck, saveAnimationChangesOnDatabase);

/* Returns an animation */
router.get('/', loginCheck, getAnimations, loadAnimationsPage);
router.get('/:id', loginCheck, loadEditorPage);


module.exports = router;
