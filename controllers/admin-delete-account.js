const express = require('express');
const router = express.Router();

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Models
let User = require('../models/user');
let UserLesson = require('../models/userlesson');

//Logged-In and  Admin Check
let adminLoginCheck = function (req, res, next) {
  console.log('adminLoginCheck in admin-delete-account');
  let adminId = req.session.userid;
  let userId = req.query.userId;

  if (adminId && req.session.isAdmin == 1 && userId) {
    console.log('is an Admin');
    res.locals.userId = userId;
    next();
  } else {
    res.redirect('/');
  }
}

let deleteAccount = function (req, res, next) {
  let userId = res.locals.userId;

  let user = new User();
  try {
    user.getById(userId, (row) => {
      user.id = row.UserId;
      user.username = row.Username;
      user.email = row.Email;
      user.password = row.Password;
      user.isAdmin = row.IsAdmin;

      console.log(user);

      // remove user from database
      user.remove(function () {
        //Delete UserLesson Entries of this User
        let userLesson = new UserLesson();
        userLesson.removeAllByUserId(userId, function () {
          console.log('redirecting...');
          res.redirect('/admin');
        });

      });

    });

  } catch (e) {
    console.log(e);
  }

}

router.get('/', adminLoginCheck, deleteAccount);

module.exports = router;
