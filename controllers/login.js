const express = require('express');
const router = express.Router();

// BCrypt
const bcrypt = require('bcrypt');

// Session Management
const session = require('../managers/session-manager');
router.use(session.getSession());

// Body Parser
const parser = require('../managers/parsing-manager');
router.use(parser.getParser());

// User
let User = require('../models/user');

const evalPassword = function (req, res, next) {
  let user = res.locals.user;
  let password = res.locals.password;

  if (user.id != 0) {
    console.log("user id ist ungleich 0");
    bcrypt.compare(password, user.password, (err, response) => {
      console.log("bcrypt hassle started");
      if (err) {
        err.httpStatusCode = 500;
        return next(err);
      }

      if (response) {
        // Passwords match
        console.log("It's a match!");

        req.session.userid = user.id;
        req.session.isAdmin = user.isAdmin;

      } else {
        // Passwords don't match
        console.log("Passwords don't match");
      }

      res.redirect('/');

    });
  } else {
    console.log('E-Mail not in DB - Redirect to Start.');
    res.redirect('/');
  }
}

const funcLogin = function (req, res, next) {
  console.log('Post auf /login');

  let email = req.body.email;
  let password = req.body.password;

  let user = new User();
  try {
    user.getByEmail(email, (row) => {
      if (row) {
        user.id = row.UserId;
        user.username = row.Username;
        user.email = row.Email;
        user.password = row.Password;
        user.isAdmin = row.IsAdmin;

        console.log(user);
        res.locals.user = user;
        res.locals.password = password;

        next();
      } else {
        res.redirect('/');
      }
    });

  } catch (e) {
    console.log(e);
  }
}

router.post('/', funcLogin, evalPassword);

module.exports = router;
